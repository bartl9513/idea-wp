<?php

/*

Template Name: Header

*/

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Raleway:400,700" rel="stylesheet">
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
 <div class="hamburger"><i class="fas fa-bars"></i></div>
  <div class="mobile-menu">
       <ul>
          
           <a href="<?php the_field('mobiledemo1'); ?>" class="mobile-href"><li>Home</li></a>
           <a href="<?php the_field('mobiledemo2'); ?>" class="mobile-href"><li>About the company</li></a>
           <a href="<?php the_field('mobiledemo3'); ?>" class="mobile-href"><li>Job offers</li></a>
           <a href="<?php the_field('mobiledemo4'); ?>" class="mobile-href"><li>Projects</li></a>
           <a href="<?php the_field('mobiledemo5'); ?>" class="mobile-href"><li>Contact</li></a>
       </ul>
       <div class="scroll-to-close">scroll to close<div class="menu-down">
       <i class="fas fa-chevron-down"></i>
       </div>
       </div>
   </div>
   <nav class="links">
        <div class="logo">
            <a href="<?php the_field('navimage-link'); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/1_03.jpg" alt="network" class="lazy"></a>
        </div>
        <ul class="menulinks">
<!--
           <a href="index.html"><li>Home</li></a>
           <a href="about.html"><li>About the company</li></a>
           <a href="joboffers.html"><li>Job offers</li></a>
           <a href="projects.html"><li>Projects</li></a>
           <a href="contact.html"><li>Contact</li></a>
-->
       
        </ul>
           <?php
    
    $defaults = array('menu' => '',
                      'container' => 'ul', 
                      'container_class' => 'menulinks', 
                      'container_id' => '', 
                      'menu_class' => 'menu', 
                      'menu_id' => false,
                      'echo' => true, 
                      'fallback_cb' => 'wp_page_menu', 
                      'before' => '', 
                      'after' => '', 
                      'link_before' => '', 
                      'link_after' => '', 
                      'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 
                      'item_spacing' => 'preserve',
                      'depth' => 0, 'walker' => '', 
                      'theme_location' => '' );
    
                        wp_nav_menu($defaults);
    
            ?>
            
        
    </nav>
</body>
