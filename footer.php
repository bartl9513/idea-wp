    <footer>
        <div class="contact-container">
            <div class="contact-footer">
                <p>contact details</p>
                <div class="next">
                    <div class="sprite1"></div>
                    <p>Wołodyjowskiego 48A<br>02-736 Warszawa</p>
                </div>
            </div>
            <div class="contact-footer middle">
                <div class="next">
                    <div class="sprite2"></div>
                    <p>Tel.: +48 22 118 44 12</p>
                </div>
                <div class="next">
                    <div class="sprite3"></div>
                    <p>E-mail: office@idea.edu.pl</p>
                </div>
            </div>
            <div class="contact-footer">
                <p>location</p>
                <div class="next">
                    <div class="sprite1"></div>
                    <p>Interdisciplinary Division for Energy Analyses<br>Wołodyjowskiego 48A, 02-736 Warszawa</p>
                </div>
            </div>
        </div>
        <div class="copy">Copyright all rights reserved 2018 tworzenie stron www.milleniumstudio.pl</div>
    </footer>


<?php wp_footer(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.counterup.min.js' ?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/waypoints.min.js' ?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/idea.js' ?>"></script>





</body>
</html>
