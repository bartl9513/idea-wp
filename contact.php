<?php

/*

Template Name: Contact

*/

get_header(); ?>

    <div class="triangle">
    
            
    <section class="contact container-fluid">
        <h1 class="contact-title">Contact</h1>
        <div class="row justify-content-center sub-contact">
            <div class="col-11 col-sm-6">
                <p><span>Adres: </span>Wołodyjowskiego 48A, 02-736 Warszawa</p>
                <p><span>Tel.: </span>+48 22 128 44 12</p>
                <p><span>E-mail: </span>office@idea.edu.pl</p>
                <?php the_field('contact_text'); ?></div>
            <div class="col-11 col-sm-6"><div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2446.722481639984!2d21.02053131579467!3d52.17573897975047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spl!2spl!4v1529314935232" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div></div>
        </div>
        
        <h1 class="contact-title">Contact form</h1>
        <div class="row justify-content-center sub-contact">
            <div class="col-11 col-sm-6">
                <textarea placeholder="Text..."></textarea>
            </div>
            <div class="col-11 col-sm-6 name-info">
                <div class="row name-row">
                    <div class="col-12 col-sm-6 name-column">
                        <input class="contact-form" type="text" placeholder="Name">
                    </div>
                    <div class="col-12 col-sm-6 name-column">
                        <input class="contact-form" type="text" placeholder="Surname">
                    </div>
                </div>
                <div class="row name-row">
                    <div class="col-12 col-sm-6 name-column">
                        <input class="contact-form" type="email" placeholder="E-mail address">
                    </div>
                    <div class="col-12 col-sm-6 name-column">
                        <input class="contact-form" type="text" placeholder="Phone number">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row justify-content-center sub-contact">
            <div class="col-11 col-sm-6">    
                <label class="container">Wyrażam wszystkie poniższe zgody
                    <input type="radio" name="radio" class="yellow-input">
                    <span class="yellow"></span>
                </label>
                <label class="container">Wyrażam zgodę na przetwarzanie moich danych osobowych
                    <input type="radio" name="radio1" class="yellow-input">
                    <span class="yellow"></span>
                </label>
                <label class="container">Wyrażam zgodę na wysyłanie mi informacji marketingowych i ofert handlowych
                    <input type="radio" name="radio2" class="yellow-input">
                    <span class="yellow"></span>
                </label>
                <label class="container">Wyrażam zgodę na prowadzenie marketingu bezpośredniego
                    <input type="radio" name="radio3" class="yellow-input">
                    <span class="yellow"></span>
                </label>
            </div>
            <div class="col-11 col-sm-6 name-info">
                <div class="row captcha-row">
                    <div class="col-12 col-sm-6 captcha-column">
                        <div class="captcha"></div>
                    </div>
                    <div class="col-12 col-sm-6 send-column">
                        <button type="submit" class="send">Send message &nbsp;<span><i class="far fa-envelope"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
    </section>
        
    </div>


<?php get_footer(); ?>
