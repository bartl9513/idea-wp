<?php

/*

Template Name: JobOffers

*/

get_header(); ?>

<main class="offers-container">

    <div class="container-fluid">
       <h1 class="offers-title">Job offers</h1>
            
            
<?php if( have_rows('job_offer') ): ?>

	<div class="row offer-row">

	<?php while( have_rows('job_offer') ): the_row(); 

		// vars
		$content = get_sub_field('job_name');
		$content2 = get_sub_field('job_description');
		$link = get_sub_field('job_link');

		?>

		<div class="col-12 col-sm-6 offer">

			<?php if( $link ): ?>
				<a href="<?php echo $link; ?>">
			<?php endif; ?>
        
	        <p><?php echo $content; ?></p></a>
	        <p><?php echo $content2; ?></p>

			<?php if( $link ): ?>
				
				
	        
			<?php endif; ?>

		   

		</div>

	<?php endwhile; ?>

	</div>

<?php endif; ?>
             
            
<!--
            <div class="col-12 col-sm-6 offer">
                <p>Social Media Manager</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.</p>
            </div>
        </div>
        <div class="row offer-row">
            <div class="col-12 col-sm-6 offer">
                <p>Consultant</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.</p>
            </div>
            <div class="col-12 col-sm-6 offer">
                <p>Energy Advisor</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.</p>
            </div>
        </div>
        <div class="row offer-row">
            <div class="col-12 col-sm-6 offer">
                <p>Intern</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.</p>
            </div>
            <div class="col-12 col-sm-6 offer">
                <p>Research Assistant</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.</p>
            </div>
        </div>
        <div class="row offer-row">
            <div class="col-12 col-sm-6 offer">
                <p>Managing Director</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.</p>
            </div>
            <div class="col-12 col-sm-6 offer">
                <p>Data Analyst</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.</p>
            </div>
        </div>
-->
    </div>
</main>


<?php get_footer(); ?>
