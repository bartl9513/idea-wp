<?php

/*

Template Name: Home

*/

get_header(); ?>
       <div class="triangle">
        <main class="home-main">
            <div class="slide-container">
               <div class="slide"><?php the_field('slide1'); ?></div>
               <div class="slide"><?php the_field('slide2'); ?></div>
               <div class="slide"><?php the_field('slide3'); ?></div>
               <div class="slide"><?php the_field('slide4'); ?></div>
               <div class="slide"><?php the_field('slide5'); ?></div>
            </div>
            
<!--            <div class="container-fluid">-->
                <div class="services">
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_09.png" alt="icon">
                        <p><?php the_field('grey1'); ?></p>
                    </div>
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_14.png" alt="icon">
                        <p><?php the_field('grey2'); ?></p>
                    </div>
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_11.png" alt="icon">
                        <p><?php the_field('grey3'); ?></p>
                    </div>
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_17.png" alt="icon">
                        <p><?php the_field('grey4'); ?></p>
                    </div>
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_19.png" alt="icon">
                        <p><?php the_field('grey5'); ?></p>
                    </div>
                </div>
<!--            </div>-->
        </main>
    </div>
    <section class="about container-fluid">
        <h1 class="about-title">About IDEA</h1>
        <div class="about-3boxes row">
            <div class="about-box col-sm-4 col-11"><?php the_field('photoleft'); ?></div>
            <div class="about-box2 col-sm-4 col-11">
            <img src="<?php bloginfo('template_directory'); ?>/img/1_40.jpg" alt="moving stairs"></div>
            <div class="about-box col-sm-4 col-11"><?php the_field('photoright'); ?></div>
        </div>
    </section>
    <section class="projects container-fluid">
        <h1>Projects</h1>
        
        <div class="projects-boxes row">
            <div class="projects-box col-sm-4 col-12">
              
              <img src="<?php the_field('projectimage1'); ?>" alt="" />
                
                <p><?php the_field('project1'); ?></p>
                <a href="<?php the_field('project1_link'); ?>"><p><?php the_field('project1-linkname'); ?></p></a>
            </div>
            <div class="projects-box col-sm-4 col-12">
               
               <img src="<?php the_field('projectimage2'); ?>" alt="" />
                
                <p><?php the_field('project2'); ?></p>
                <a href="<?php the_field('project2_link'); ?>"><p><?php the_field('project2-linkname'); ?></p></a>
            </div>
            <div class="projects-box col-sm-4 col-12">
               
               <img src="<?php the_field('projectimage3'); ?>" alt="" />
                
                <p><?php the_field('project3'); ?></p>
                <a href="<?php the_field('project3_link'); ?>"><p><?php the_field('project3-linkname'); ?></p></a>
            </div>
        </div>
    </section>
    <section id="numbersoffset" class="numbers">
       <div class="whiteline"></div>
        <h1>IDEA in numbers</h1>
        <div class="circles container-fluid" id="circles-goto">   
<!--            <div class="circle-drop">-->
                <div class="circlerow row">
                        <div class="col-6 col-sm-3 center-block circle-col">
                            <div class="circle">
                                <svg class="svg-cont" height="100%" width="100%">
                                    <circle class="blackcircle blackcircle1" cx="50%" cy="50%" r="47%"/>
                                </svg>
                            <div class="svg-circle">5</div><span>&nbsp;tys.</span>
                            <div class="circle-text">of lorem ipsum something</div>
                            </div>
                        </div>
                        
                        <div class="col-6 col-sm-3 center-block circle-col">
                            <div class="circle">
                                <svg class="svg-cont" height="100%" width="100%">
                                    <circle class="blackcircle blackcircle2" cx="50%" cy="50%" r="47%"/>
                                </svg>
                            <div class="svg-circle">7</div><span>&nbsp;mln.</span>
                            <div class="circle-text">of lorem ipsum something</div>
                            </div>
                        </div>
                    
                        <div class="col-6 col-sm-3 center-block circle-col">
                            <div class="circle">
                                <svg class="svg-cont" height="100%" width="100%">
                                    <circle class="blackcircle blackcircle3" cx="50%" cy="50%" r="47%"/>
                                </svg>
                            <div class="svg-circle">60</div><span>&nbsp;mln.</span>
                            <div class="circle-text">of lorem ipsum something</div>
                            </div>
                        </div>
                        
                        <div class="col-6 col-sm-3 center-block circle-col">
                            <div class="circle">
                                <svg class="svg-cont" height="100%" width="100%">
                                    <circle class="blackcircle blackcircle4" cx="50%" cy="50%" r="47%"/>
                                </svg>
                            <div class="svg-circle">479</div><span>&nbsp;mln.</span>
                            <div class="circle-text">of lorem ipsum something</div>
                            </div>
                        </div>
                </div>
<!--            </div>-->
        </div>
    </section>

<?php get_footer(); ?>
