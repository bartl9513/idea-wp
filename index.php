<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package Idea
*/

get_header(); ?>
      
       <div class="triangle">
        <main class="home-main">
            <div class="slide-container">
               <div class="slide">Interdisciplinary division for energy analyses</div>
               <div class="slide">Lorem division for energy analyses</div>
               <div class="slide">Text Interdisciplinary Lorem Ipsum</div>
               <div class="slide">Ipsum Interdisciplinary division for energy analyses</div>
               <div class="slide">Slide Interdisciplinary division for energy</div>
            </div>
            
<!--            <div class="container-fluid">-->
                <div class="services">
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_09.png" alt="icon">
                        <p>Lorem Ipsum</p>
                    </div>
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_14.png" alt="icon">
                        <p>Lorem Ipsum</p>
                    </div>
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_11.png" alt="icon">
                        <p>Lorem Ipsum</p>
                    </div>
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_17.png" alt="icon">
                        <p>Lorem Ipsum</p>
                    </div>
                    <div class="services-img">
                        <img src="<?php bloginfo('template_directory'); ?>/img/1_19.png" alt="icon">
                        <p>Lorem Ipsum</p>
                    </div>
                </div>
<!--            </div>-->
        </main>
    </div>
    <section class="about container-fluid">
        <h1 class="about-title">About IDEA</h1>
        <div class="about-3boxes row">
            <div class="about-box col-sm-4 col-11"><?php the_field('example12'); ?></div>
            <div class="about-box2 col-sm-4 col-11">
            <img src="<?php bloginfo('template_directory'); ?>/img/1_40.jpg" alt="moving stairs"></div>
            <div class="about-box col-sm-4 col-11">iLorem ipsum dolor sit amet, consectetur adipisicing elit. Natus in, dicta ipsa. Consequatur voluptates, consectetur tempore molestias qui aliquid delectus nam, assumenda molestiae. Doloribus nesciunt, repellendus excepturi qui minima illo! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique odio molestiae nihil quo dolorum! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus in, dicta ipsa. Consequatur voluptates, consectetur tempore molestias qui aliquid delectus nam, assumenda molestiae.</div>
        </div>
    </section>
    <section class="projects container-fluid">
        <h1>Projects</h1>
        <div class="projects-boxes row">
            <div class="projects-box col-sm-4 col-12">
                <img src="<?php bloginfo('template_directory'); ?>/img/1_27.jpg" alt="icon">
                <p>Project Lorem ipsum</p>
                <p>see more</p>
            </div>
            <div class="projects-box col-sm-4 col-12">
                <img src="<?php bloginfo('template_directory'); ?>/img/1_29.jpg" alt="icon">
                <p>Project Lorem ipsum</p>
                <p>see more</p>
            </div>
            <div class="projects-box col-sm-4 col-12">
                <img src="<?php bloginfo('template_directory'); ?>/img/1_31.jpg" alt="icon">
                <p>Project Lorem ipsum</p>
                <p>see more</p>
            </div>
        </div>
    </section>
    <section id="numbersoffset" class="numbers">
       <div class="whiteline"></div>
        <h1>IDEA in numbers</h1>
        <div class="circles container-fluid" id="circles-goto">   
<!--            <div class="circle-drop">-->
                <div class="circlerow row">
                        <div class="col-6 col-sm-3 center-block circle-col">
                            <div class="circle">
                                <svg class="svg-cont" height="100%" width="100%">
                                    <circle class="blackcircle blackcircle1" cx="50%" cy="50%" r="47%"/>
                                </svg>
                            <div class="svg-circle">5</div><span>&nbsp;tys.</span>
                            <div class="circle-text">of lorem ipsum something</div>
                            </div>
                        </div>
                        
                        <div class="col-6 col-sm-3 center-block circle-col">
                            <div class="circle">
                                <svg class="svg-cont" height="100%" width="100%">
                                    <circle class="blackcircle blackcircle2" cx="50%" cy="50%" r="47%"/>
                                </svg>
                            <div class="svg-circle">7</div><span>&nbsp;mln.</span>
                            <div class="circle-text">of lorem ipsum something</div>
                            </div>
                        </div>
                    
                        <div class="col-6 col-sm-3 center-block circle-col">
                            <div class="circle">
                                <svg class="svg-cont" height="100%" width="100%">
                                    <circle class="blackcircle blackcircle3" cx="50%" cy="50%" r="47%"/>
                                </svg>
                            <div class="svg-circle">60</div><span>&nbsp;mln.</span>
                            <div class="circle-text">of lorem ipsum something</div>
                            </div>
                        </div>
                        
                        <div class="col-6 col-sm-3 center-block circle-col">
                            <div class="circle">
                                <svg class="svg-cont" height="100%" width="100%">
                                    <circle class="blackcircle blackcircle4" cx="50%" cy="50%" r="47%"/>
                                </svg>
                            <div class="svg-circle">479</div><span>&nbsp;mln.</span>
                            <div class="circle-text">of lorem ipsum something</div>
                            </div>
                        </div>
                </div>
<!--            </div>-->
        </div>
    </section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
