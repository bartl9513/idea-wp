<?php

/*

Template Name: Projects

*/

get_header(); ?>

<div class="triangle">
    
            
    <section class="about2 container-fluid">
        <h1 class="about-title about-title2">Projects</h1>
        
        
        
        <?php if( have_rows('project_template') ): ?>

	<div class="container-fluid">

	<?php while( have_rows('project_template') ): the_row(); 

		// vars
        $name1 = get_sub_field('project1_name');
		$text1 = get_sub_field('project1_description');
		$img1 = get_sub_field('project1_image');

		?>

		<div class="row justify-content-center row-reverse">

			<?php if( $link ): ?>
				<a href="<?php echo $link; ?>"></a>
			<?php endif; ?>
	        
	        <div class="col-11 col-sm-6 sub-projects"><img src="<?php echo $img1['url']; ?>" alt="<?php echo $img1['alt'] ?>" /></div>
            <div class="col-11 col-sm-6 sub-projects">
            <p><?php echo $name1; ?></p><?php echo $text1; ?>
            </div>

			<?php if( $link ): ?>
				
				
	        
			<?php endif; ?>

		   

		</div>

	<?php endwhile; ?>

	</div>

<?php endif; ?>
       
       
       
       

        
    </section>
    </div>


<?php get_footer(); ?>
