-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 19 Cze 2018, 11:17
-- Wersja serwera: 5.6.37
-- Wersja PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `ideaDB`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Komentator WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-06-13 15:43:00', '2018-06-13 13:43:00', 'Cześć, to jest komentarz.\nAby zapoznać się z moderowaniem, edycją i usuwaniem komentarzy, należy odwiedzić ekran Komentarze w kokpicie.\nAwatary komentujących pochodzą z <a href="https://pl.gravatar.com">Gravatara</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/idea2', 'yes'),
(2, 'home', 'http://localhost/idea2', 'yes'),
(3, 'blogname', 'idea', 'yes'),
(4, 'blogdescription', 'Kolejna witryna oparta na WordPressie', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'bartek9513@interia.pl', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j F Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:89:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:34:"advanced-custom-fields-pro/acf.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:71:"C:\\Program Files (x86)\\Ampps\\www\\idea2/wp-content/themes/idea/style.css";i:1;s:0:"";}', 'no'),
(40, 'template', 'idea', 'yes'),
(41, 'stylesheet', 'idea', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Europe/Warsaw', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'initial_db_version', '38590', 'yes'),
(93, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(94, 'fresh_site', '0', 'yes'),
(95, 'WPLANG', 'pl_PL', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'cron', 'a:6:{i:1529408582;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1529415782;a:1:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1529415783;a:2:{s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1529415817;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1529415851;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(112, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1528898069;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(123, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:65:"https://downloads.wordpress.org/release/pl_PL/wordpress-4.9.6.zip";s:6:"locale";s:5:"pl_PL";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/pl_PL/wordpress-4.9.6.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.6";s:7:"version";s:5:"4.9.6";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1529390901;s:15:"version_checked";s:5:"4.9.6";s:12:"translations";a:0:{}}', 'no'),
(124, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1529390907;s:7:"checked";a:4:{s:4:"idea";s:5:"1.0.0";s:13:"twentyfifteen";s:3:"2.0";s:15:"twentyseventeen";s:3:"1.6";s:13:"twentysixteen";s:3:"1.5";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(126, '_site_transient_timeout_browser_143156aedca8214ce63a15cbea76913a', '1529502218', 'no'),
(127, '_site_transient_browser_143156aedca8214ce63a15cbea76913a', 'a:10:{s:4:"name";s:6:"Chrome";s:7:"version";s:12:"67.0.3396.87";s:8:"platform";s:7:"Windows";s:10:"update_url";s:29:"https://www.google.com/chrome";s:7:"img_src";s:43:"http://s.w.org/images/browsers/chrome.png?1";s:11:"img_src_ssl";s:44:"https://s.w.org/images/browsers/chrome.png?1";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
(137, 'can_compress_scripts', '1', 'no'),
(140, 'current_theme', 'Idea', 'yes'),
(141, 'theme_mods_idea', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:4:"main";i:2;}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(142, 'theme_switched', '', 'yes'),
(143, 'category_children', 'a:0:{}', 'yes'),
(149, 'recently_activated', 'a:0:{}', 'yes'),
(150, 'acf_version', '5.6.10', 'yes'),
(171, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(210, '_site_transient_timeout_community-events-d41d8cd98f00b204e9800998ecf8427e', '1529434147', 'no'),
(211, '_site_transient_community-events-d41d8cd98f00b204e9800998ecf8427e', 'a:2:{s:8:"location";a:1:{s:2:"ip";b:0;}s:6:"events";a:2:{i:0;a:7:{s:4:"type";s:8:"wordcamp";s:5:"title";s:16:"WordCamp Poznań";s:3:"url";s:32:"https://2018.poznan.wordcamp.org";s:6:"meetup";N;s:10:"meetup_url";N;s:4:"date";s:19:"2018-07-06 00:00:00";s:8:"location";a:4:{s:8:"location";s:15:"Poznań, Poland";s:7:"country";s:2:"PL";s:8:"latitude";d:52.415396600000001;s:9:"longitude";d:16.925032099999999;}}i:1;a:7:{s:4:"type";s:8:"wordcamp";s:5:"title";s:13:"WordCamp Brno";s:3:"url";s:30:"https://2018.brno.wordcamp.org";s:6:"meetup";N;s:10:"meetup_url";N;s:4:"date";s:19:"2018-10-20 00:00:00";s:8:"location";a:4:{s:8:"location";s:20:"Brno, Czech Republic";s:7:"country";s:2:"CZ";s:8:"latitude";d:49.206563600000003;s:9:"longitude";d:16.592329299999999;}}}}', 'no'),
(234, '_site_transient_timeout_theme_roots', '1529392706', 'no'),
(235, '_site_transient_theme_roots', 'a:4:{s:4:"idea";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no'),
(236, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1529399750;s:8:"response";a:1:{s:19:"akismet/akismet.php";O:8:"stdClass":12:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"4.0.7";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.4.0.7.zip";s:5:"icons";a:2:{s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";}s:7:"banners";a:1:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.6";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}}s:12:"translations";a:0:{}s:9:"no_update";a:1:{s:9:"hello.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/hello-dolly";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";s:5:"icons";a:2:{s:2:"2x";s:63:"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907";s:2:"1x";s:63:"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907";}s:7:"banners";a:1:{s:2:"1x";s:65:"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(237, '_transient_timeout_feed_a421d6f32723068ab074a40017a9e1f9', '1529434148', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(238, '_transient_feed_a421d6f32723068ab074a40017a9e1f9', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n\n\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:49:"\n	\n	\n	\n	\n	\n	\n	\n	\n	\n	\n		\n		\n		\n		\n		\n		\n		\n		\n		\n	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:6:"Polska";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:24:"https://pl.wordpress.org";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:34:"\n	Sat, 26 May 2018 08:28:57 +0000	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"pl-PL";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:40:"https://wordpress.org/?v=5.0-alpha-43320";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:45:"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"Program WordCamp Poznań 2018 już tu jest!";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:77:"https://pl.wordpress.org/2018/05/26/program-wordcamp-poznan-2018-juz-tu-jest/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:86:"https://pl.wordpress.org/2018/05/26/program-wordcamp-poznan-2018-juz-tu-jest/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 26 May 2018 08:28:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"Wydarzenia";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"wordcamp";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://pl.wordpress.org/?p=1817";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:395:"Pewnie większość z was już wie, że w zeszłym tygodniu opublikowaliśmy program tegorocznego WordCampa, który odbędzie się w dniach 6-8 lipca w Poznaniu. WordCamp Poznań 2018 zbliża się dużymi krokami! Jeśli nie masz jeszcze biletu, to nie zastanawiaj się już dłużej, tylko kup bilet na najważniejsze WordPressowe wydarzenie 2018 roku! Zapraszamy do zapoznania się z [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:18:"Katarzyna Gajewska";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2455:"<p>Pewnie większość z was już wie, że w zeszłym tygodniu opublikowaliśmy program tegorocznego WordCampa, który odbędzie się w dniach <strong>6-8 lipca w Poznaniu</strong>.</p>\n<p>WordCamp Poznań 2018 zbliża się dużymi krokami! Jeśli nie masz jeszcze biletu, to nie zastanawiaj się już dłużej, tylko <a href="https://2018.poznan.wordcamp.org/bilety/" target="_blank" rel="noopener noreferrer"><strong>kup bilet</strong></a> na najważniejsze WordPressowe wydarzenie 2018 roku! Zapraszamy do zapoznania się z <a href="https://2018.poznan.wordcamp.org/program/" target="_blank" rel="noopener noreferrer"><strong>pełnym programem konferencji</strong></a> i do zapisywania się na warsztaty oraz Contributor Day.</p>\n<p>Startujemy 6 lipca! Pierwszego dnia konferencji po prostu nie można przegapić. Zaczniemy od bardziej praktycznych wyzwań. Każdy uczestnik będzie miał możliwość wzięcia udziału w ciekawych warsztatach, które będą świetną okazją, aby zdobyć nowe umiejętności i poznać ciekawych ludzi. W tym roku zaserwujemy aż 6 różnych warsztatów. Warto zapoznać się z tematami i już dziś zarezerwować sobie miejsce. Równolegle do warsztatów będzie odbywał się <a href="https://2018.poznan.wordcamp.org/czym-jest-contributor-day/" target="_blank" rel="noopener noreferrer"><strong>Contributor Day</strong></a>, podczas którego będzie można dołączyć do innych entuzjastów i popracować nad kodem, tłumaczeniem lub zająć się odpowiadaniem na pytania na forum wsparcia. Z pewnością każdy znajdzie tutaj coś dla siebie!</p>\n<p>Kolejne dwa dni konferencji będą wypełnione ciekawymi prelekcjami, na których nie może cię zabraknąć! Tematy są podzielone na dwie ścieżki: ogólną i techniczną. Ścieżkę ogólną polecamy wszystkim, którzy są zainteresowani tematami związanymi z wykorzystaniem WordPressa, blogowaniem i innymi zagadnieniami. Będziecie mieli okazję dowiedzieć się więcej o dostępności, pracy zdalnej, wycenie projektów czy analityce. Z kolei ścieżkę techniczną polecamy uczestnikom poszukującym bardziej specjalistycznej wiedzy. Będzie to świetna okazja, żeby posłuchać prelekcji na temat backupu, automatyzacji zadań czy zabezpieczenia WordPressa. Tematyka zarówno ścieżki ogólnej, jak i technicznej, jest bardzo zróżnicowana. Bez wątpienia każdy znajdzie coś, co go zainteresuje!</p>\n<p>Do zobaczenia w Poznaniu!</p>\n<p>&nbsp;</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:82:"https://pl.wordpress.org/2018/05/26/program-wordcamp-poznan-2018-juz-tu-jest/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:42:"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:55:"WordPress 4.9.4 – Wydanie poprawiające ważny błąd";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:93:"https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 06 Feb 2018 18:25:48 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:7:"Wydanie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://pl.wordpress.org/?p=1539";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:489:"WordPress 4.9.4 jest już dostępny. To wydanie rozwiązuje istotny problem wprowadzony w wersji 4.9.3, który powodował, że strony obsługujące automatyczne aktualizacje nie będą mogły ich przeprowadzić. Wymagane jest ręczne zaktualizowanie WordPressa do wersji 4.9.4 przez Ciebie lub Twojego dostawcę hostingu. Cztery lata temu, w wydaniu 3.7 &#8222;Basie&#8221; WordPressa wprowadziliśmy możliwość automatycznej aktualizacji systemu, dzięki czemu strony stały [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:37:"Krzysztof Trynkiewicz (Sukces Strony)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2631:"<p>WordPress 4.9.4 jest już dostępny.</p>\n<p>To wydanie rozwiązuje istotny problem wprowadzony w wersji 4.9.3, który powodował, że strony obsługujące automatyczne aktualizacje nie będą mogły ich przeprowadzić. <strong><span style="text-decoration: underline">Wymagane jest ręczne zaktualizowanie WordPressa</span> do wersji 4.9.4 przez Ciebie lub Twojego dostawcę hostingu.</strong></p>\n<p><span id="more-1539"></span></p>\n<p>Cztery lata temu, w wydaniu <a href="https://wordpress.org/news/2013/10/basie/">3.7 &#8222;Basie&#8221; WordPressa</a> wprowadziliśmy możliwość automatycznej aktualizacji systemu, dzięki czemu strony stały się bezpieczniejsze i mniej awaryjne &#8211; nawet, jeśli samodzielnie nie miałeś możliwości ich aktualizować. Przez te 4 lata mechanizm ten pomógł milionom witryn, aktualizując je do bieżących wersji i naprawiając bardzo sporadyczne usterki. Niestety we wczorajszym <a href="https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/">wydaniu 4.9.3</a> znalazł się ważny błąd, który został wykryty dopiero po upublicznieniu nowej wersji systemu. Błąd ten powoduje, że WordPress nie zaktualizuje się automatycznie do wersji 4.9.4, co oznacza, że aktualizację taką trzeba przeprowadzić ręcznie z poziomu panelu administracyjnego lub z pomocą dostawcy serwera hostingowego.</p>\n<p><strong>Aby przeprowadzić ręczną aktualizację, udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Dopóki tego nie wykonasz, WordPress <span style="text-decoration: underline">nie będzie aktualizował się automatycznie do kolejnych wersji</span>. Zalecamy, by zrobić to natychmiast.</strong></p>\n<p>Dostawcy hostingu, którzy aktualizują automatycznie WordPressa w imieniu swoich klientów, mogą przystąpić do aktualizacji w standardowym trybie. Będziemy ściśle współpracować z innymi dostawcami hostingu, by jak najwięcej ich klientów otrzymało tą aktualizację.</p>\n<p><a href="https://make.wordpress.org/core/2018/02/06/wordpress-4-9-4-release-the-technical-details/">Więcej szczegółów technicznych opublikowaliśmy na naszym blogu o tworzeniu rdzenia WordPressa</a>. Pełna lista zmian dostępna jest w <a href="https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.9.4&amp;group=component">spisie zamkniętych wątków</a>.</p>\n<p><a href="https://pl.wordpress.org/">Pobierz WordPressa 4.9.4</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:89:"https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"6";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:42:"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:49:"WordPress 4.9.3 – Wydanie poprawiające błędy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:79:"https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:87:"https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/#respond";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 06 Feb 2018 10:50:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:7:"Wydanie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://pl.wordpress.org/?p=1536";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:450:"WordPress 4.9.3 jest już dostępny. To wydanie zawiera poprawki 34 błędów obecnych w wersji 4.9 WordPressa, w tym dotyczących zestawów zmian na ekranie Personalizacji, widgetów, edytora wizualnego i kompatybilności z PHP 7.2.  Spis wszystkich zmian znajdziesz w liście zamkniętych wątków oraz liście przeprowadzonych zmian. Pobierz WordPressa 4.9.3 lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:37:"Krzysztof Trynkiewicz (Sukces Strony)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3596:"<p>WordPress 4.9.3 jest już dostępny.</p>\n<p>To wydanie zawiera poprawki 34 błędów obecnych w wersji 4.9 WordPressa, w tym dotyczących zestawów zmian na ekranie Personalizacji, widgetów, edytora wizualnego i kompatybilności z PHP 7.2.  Spis wszystkich zmian znajdziesz<span style="line-height: 1.5"> w <a href="https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.9.3&amp;group=component">liście zamkniętych wątków</a> oraz <a href="https://core.trac.wordpress.org/log/branches/4.9?rev=42630&amp;stop_rev=42521">liście przeprowadzonych zmian</a>.</span></p>\n<p><span id="more-1536"></span></p>\n<p><a href="https://pl.wordpress.org/">Pobierz WordPressa 4.9.3</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do powstania wersji 4.9.3:</p>\n<p><a href="https://profiles.wordpress.org/jorbin/">Aaron Jorbin</a>, <a href="https://profiles.wordpress.org/abdullahramzan/">abdullahramzan</a>, <a href="https://profiles.wordpress.org/adamsilverstein/">Adam Silverstein</a>, <a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/andreiglingeanu/">andreiglingeanu</a>, <a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>, <a href="https://profiles.wordpress.org/bpayton/">Brandon Payton</a>, <a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>, <a href="https://profiles.wordpress.org/coleh/">coleh</a>, <a href="https://profiles.wordpress.org/darko-a7/">Darko A7</a>, <a href="https://profiles.wordpress.org/desertsnowman/">David Cramer</a>, <a href="https://profiles.wordpress.org/dlh/">David Herrera</a>, <a href="https://profiles.wordpress.org/dd32/">Dion Hulse</a>, <a href="https://profiles.wordpress.org/flixos90/">Felix Arntz</a>, <a href="https://profiles.wordpress.org/frank-klein/">Frank Klein</a>, <a href="https://profiles.wordpress.org/pento/">Gary Pendergast</a>, <a href="https://profiles.wordpress.org/audrasjb/">Jb Audras</a>, <a href="https://profiles.wordpress.org/jbpaul17/">Jeffrey Paul</a>, <a href="https://profiles.wordpress.org/lizkarkoski/">lizkarkoski</a>, <a href="https://profiles.wordpress.org/clorith/">Marius L. J.</a>, <a href="https://profiles.wordpress.org/mattyrob/">mattyrob</a>, <a href="https://profiles.wordpress.org/munyagu/">munyagu</a>, <a href="https://profiles.wordpress.org/ndavison/">ndavison</a>, <a href="https://profiles.wordpress.org/nickmomrik/">Nick Momrik</a>, <a href="https://profiles.wordpress.org/peterwilsoncc/">Peter Wilson</a>, <a href="https://profiles.wordpress.org/rachelbaker/">Rachel Baker</a>, <a href="https://profiles.wordpress.org/rishishah/">rishishah</a>, <a href="https://profiles.wordpress.org/othellobloke/">Ryan Paul</a>, <a href="https://profiles.wordpress.org/sasiddiqui/">Sami Ahmed Siddiqui</a>, <a href="https://profiles.wordpress.org/sayedwp/">Sayed Taqui</a>, <a href="https://profiles.wordpress.org/seanchayes/">Sean Hayes</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, <a href="https://profiles.wordpress.org/shooper/">Shawn Hooper</a>, <a href="https://profiles.wordpress.org/netweb/">Stephen Edgar</a>, <a href="https://profiles.wordpress.org/manikmist09/">Sultan Nasir Uddin</a>, <a href="https://profiles.wordpress.org/tigertech/">tigertech</a> oraz <a href="https://profiles.wordpress.org/westonruter/">Weston Ruter</a>.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:84:"https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:42:"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"WordPress 4.9.2 – Wydanie poprawiające bezpieczeństwo i błędy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:96:"https://pl.wordpress.org/2018/01/17/wordpress-4-9-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:105:"https://pl.wordpress.org/2018/01/17/wordpress-4-9-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 17 Jan 2018 11:06:38 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:7:"Wydanie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://pl.wordpress.org/?p=1512";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:467:"WordPress 4.9.2 jest już dostępny. To wydanie poprawia bezpieczeństwo we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji. Wykryto podatność na atak XSS w plikach Flash biblioteki MediaElement, która jest dołączana do WordPressa w celu zapewnienia kompatybilności ze starszym oprogramowaniem. Ponieważ pliki te nie są już potrzebne w zdecydowanej większości przypadków, zostały one usunięte [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:37:"Krzysztof Trynkiewicz (Sukces Strony)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4168:"<p>WordPress 4.9.2 jest już dostępny. <strong>To wydanie poprawia bezpieczeństwo</strong> we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji.</p>\n<p><span id="more-1512"></span></p>\n<p>Wykryto podatność na atak XSS w plikach Flash biblioteki MediaElement, która jest dołączana do WordPressa w celu zapewnienia kompatybilności ze starszym oprogramowaniem. Ponieważ pliki te nie są już potrzebne w zdecydowanej większości przypadków, zostały one usunięte z WordPressa.</p>\n<p>Biblioteka MediaElement została już udostępniona w nowej wersji, która naprawa problem. W repozytorium wtyczek WordPressa znajduje się nowa <a href="https://wordpress.org/plugins/mediaelement-flash-fallbacks/">wtyczka, przywracająca obsługę biblioteki MediaElement z poprawionymi plikami</a>.</p>\n<p>Dziękujemy zgłaszającym (<a href="https://opnsec.com">Enguerran Gillier</a> i <a href="https://widiz.com/">Widiz</a>) za <a href="https://make.wordpress.org/core/handbook/reporting-security-vulnerabilities/">odpowiedzialne ujawnienie</a> problemów z bezpieczeństwem.</p>\n<p>W WordPressie 4.9.2 rozwiązano także 21 innych problemów, w tym:</p>\n<ul>\n<li>naprawiono błędy JavaScript, które uniemożliwiały zapisywanie wpisów w Firefoxie,</li>\n<li>przywrócono wcześniejsze, niezależne od taksonomii działanie <code>get_category_link()</code> i <code>category_description()</code>,</li>\n<li>od teraz zmiana motywu spowoduje podjęcie próby przywrócenia poprzednich przypisań widgetów &#8211; nawet, jeśli w ostatnio używanym motywie nie były dostępne żadne obszary na widgety.</li>\n</ul>\n<p>W dokumentacji Codex znajdziesz <a href="https://codex.wordpress.org/Version_4.9.2">więcej informacji na temat wszystkich rozwiązanych problemów w wersji 4.9.2</a>.</p>\n<p><a href="https://pl.wordpress.org/">Pobierz WordPressa 4.9.2</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do powstania wersji 4.9.2:</p>\n<p><a href="https://profiles.wordpress.org/0x6f0/">0x6f0</a>, <a href="https://profiles.wordpress.org/jorbin/">Aaron Jorbin</a>, <a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/aduth/">Andrew Duthie</a>, <a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>, <a href="https://profiles.wordpress.org/blobfolio/">Blobfolio</a>, <a href="https://profiles.wordpress.org/boonebgorges/">Boone Gorges</a>, <a href="https://profiles.wordpress.org/icaleb/">Caleb Burks</a>, <a href="https://profiles.wordpress.org/poena/">Carolina Nymark</a>, <a href="https://profiles.wordpress.org/chasewg/">chasewg</a>, <a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>, <a href="https://profiles.wordpress.org/dd32/">Dion Hulse</a>, <a href="https://profiles.wordpress.org/hardik-amipara/">Hardik Amipara</a>, <a href="https://profiles.wordpress.org/ionvv/">ionvv</a>, <a href="https://profiles.wordpress.org/jaswrks/">Jason Caldwell</a>, <a href="https://profiles.wordpress.org/jbpaul17/">Jeffrey Paul</a>, <a href="https://profiles.wordpress.org/jeremyfelt/">Jeremy Felt</a>, <a href="https://profiles.wordpress.org/joemcgill/">Joe McGill</a>, <a href="https://profiles.wordpress.org/johnschulz/">johnschulz</a>, <a href="https://profiles.wordpress.org/juiiee8487/">Juhi Patel</a>, <a href="https://profiles.wordpress.org/obenland/">Konstantin Obenland</a>, <a href="https://profiles.wordpress.org/markjaquith/">Mark Jaquith</a>, <a href="https://profiles.wordpress.org/rabmalin/">Nilambar Sharma</a>, <a href="https://profiles.wordpress.org/peterwilsoncc/">Peter Wilson</a>, <a href="https://profiles.wordpress.org/rachelbaker/">Rachel Baker</a>, <a href="https://profiles.wordpress.org/rinkuyadav999/">Rinku Y</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a> oraz <a href="https://profiles.wordpress.org/westonruter/">Weston Ruter</a>.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:101:"https://pl.wordpress.org/2018/01/17/wordpress-4-9-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:45:"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:57:"WordPress 4.8.3 – Wydanie poprawiające bezpieczeństwo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:88:"https://pl.wordpress.org/2017/10/31/wordpress-4-8-3-wydanie-poprawiajace-bezpieczenstwo/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:97:"https://pl.wordpress.org/2017/10/31/wordpress-4-8-3-wydanie-poprawiajace-bezpieczenstwo/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 31 Oct 2017 11:26:39 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:15:"Bezpieczeństwo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:7:"Wydanie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://pl.wordpress.org/?p=1289";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:475:"WordPress 4.8.3 jest już dostępny. To wydanie poprawia bezpieczeństwo we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji. WordPress w wersji 4.8.2 i wcześniejszych jest podatny na problem z $wpdb-&#62;prepare(), który może prowadzić do tworzenia nieoczekiwanych i niebezpiecznych zapytań, umożliwiających potencjalny atak metodą SQL injection (SQLi). Rdzeń systemu nie jest bezpośrednio podatny na ten [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:37:"Krzysztof Trynkiewicz (Sukces Strony)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1522:"<p>WordPress 4.8.3 jest już dostępny. <strong>To wydanie poprawia bezpieczeństwo</strong> we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji.</p>\n<p><span id="more-1289"></span></p>\n<p>WordPress w wersji 4.8.2 i wcześniejszych jest podatny na problem z <code>$wpdb-&gt;prepare()</code>, który może prowadzić do tworzenia nieoczekiwanych i niebezpiecznych zapytań, umożliwiających potencjalny atak metodą SQL injection (SQLi). Rdzeń systemu nie jest bezpośrednio podatny na ten problem, ale dodaliśmy dodatkowe zabezpieczenia, by wtyczki i motywy nie mogły go przypadkowo powodować. Problem zgłosił <a href="https://twitter.com/ircmaxell">Anthony Ferrara</a>.</p>\n<p>To wydanie WordPressa zmienia zachowanie funkcji <code>esc_sql()</code>. Różnica nie będzie istotna dla większości programistów. Zachęcamy do <a href="https://make.wordpress.org/core/2017/10/31/changed-behaviour-of-esc_sql-in-wordpress-4-8-3/">przeczytania informacji dla deweloperów</a>.</p>\n<p>Dziękujemy zgłaszającemu za <a href="https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/">odpowiedzialne ujawnienie</a> problemu z bezpieczeństwem.</p>\n<p><a href="https://pl.wordpress.org/">Pobierz WordPressa 4.8.3</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:93:"https://pl.wordpress.org/2017/10/31/wordpress-4-8-3-wydanie-poprawiajace-bezpieczenstwo/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:42:"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:20:"WordPress 4.9 Beta 2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://pl.wordpress.org/2017/10/12/wordpress-4-9-beta-2/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:66:"https://pl.wordpress.org/2017/10/12/wordpress-4-9-beta-2/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 12 Oct 2017 07:03:53 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:14:"Wersje testowe";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://pl.wordpress.org/?p=1235";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:433:"WordPress 4.9 Beta 2 jest już dostępny! Oprogramowanie jest wciąż w fazie tworzenia, dlatego nie zalecamy jego używania w witrynie produkcyjnej. Zastanów się nad środowiskiem testowym i tam sprawdzaj do woli. Aby przetestować WordPress 4.9, wypróbuj wtyczkę WordPress Beta Tester (regularnie aktualizuje one do  &#8222;krawych nocnych wydań&#8221;) albo pobierz wersję beta (zip). Więcej informacji na temat [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Marcin Pietrzak";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1781:"<p>WordPress 4.9 Beta 2 jest już dostępny!</p>\n<p><strong>Oprogramowanie jest wciąż w fazie tworzenia</strong>, dlatego nie zalecamy jego używania w witrynie produkcyjnej. Zastanów się nad środowiskiem testowym i tam sprawdzaj do woli. Aby przetestować WordPress 4.9, wypróbuj wtyczkę <a href="https://pl.wordpress.org/plugins/wordpress-beta-tester/">WordPress Beta Tester</a> (regularnie aktualizuje one do  &#8222;krawych nocnych wydań&#8221;) albo <a href="https://wordpress.org/wordpress-4.9-beta2.zip">pobierz wersję beta</a> (zip).</p>\n<p>Więcej informacji na temat tego, co nowego w wersji 4.9, znajdziesz w blogu we wpisie <a href="https://wordpress.org/news/2017/10/wordpress-4-9-beta-1/">Beta 1</a> (po angielsku). Od tego czasu dokonaliśmy <a href="https://core.trac.wordpress.org/log/trunk/?action=stop_on_copy&amp;mode=stop_on_copy&amp;rev=41846&amp;stop_rev=41777&amp;limit=100&amp;sfp_email=&amp;sfph_mail=">70 zmian</a> w wersji Beta 2.</p>\n<p><a href="https://translate.wordpress.org/locale/pl/default/wp/dev">Pomóż nam przetłumaczyć WordPress na język polski</a>!</p>\n<p><strong>Jeśli widzisz błąd</strong>, napisz wiadomość na forum <a href="https://wordpress.org/support/forum/alphabeta">Alpha/Beta</a>. Jeżeli coś nie działa, to chcielibyśm się tego dowiedzieć właśnie do ciebie! Jeśli czujesz się na siłach, żeby napisać raport zawierający opis jak powtórzyć błąd to  skorzystaj z serwisu <a href="https://make.wordpress.org/core/reports/">WordPress Trac</a>. Na nim też znajdziesz <a href="https://core.trac.wordpress.org/tickets/major">listę znanych błędów</a>.</p>\n<p><em>Przetestujmy razem:</em><br />\n<em> edytowanie kodu, przełączniki tematyczne,</em><br />\n<em> widgety, harmonogramowanie.</em></p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:62:"https://pl.wordpress.org/2017/10/12/wordpress-4-9-beta-2/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"3";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:45:"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"WordPress 4.8.2 – Wydanie poprawiające bezpieczeństwo i błędy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:96:"https://pl.wordpress.org/2017/09/22/wordpress-4-8-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:104:"https://pl.wordpress.org/2017/09/22/wordpress-4-8-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/#respond";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 22 Sep 2017 11:37:20 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:15:"Bezpieczeństwo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:7:"Wydanie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://pl.wordpress.org/?p=1213";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:476:"WordPress 4.8.2 jest już dostępny. Wersja poprawia bezpieczeństwo wszystkich poprzednich wersji i zdecydowanie zachęcamy do natychmiastowej aktualizacji witryn. WordPress w wersji 4.8.1 i starsze mają następujące problemy z bezpieczeństwem: $wpdb-&#62; prepare() mogło tworzyć nieoczekiwane i niebezpieczne zapytania prowadzące do potencjalnego wstrzyknięcia kod SQL (SQLi). Core WordPressa nie jest bezpośrednio narażony na ten problem, ale poprawiono [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Marcin Pietrzak";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3051:"<p>WordPress 4.8.2 jest już dostępny. Wersja <strong>poprawia bezpieczeństwo</strong> wszystkich poprzednich wersji i zdecydowanie zachęcamy do natychmiastowej aktualizacji witryn.</p>\n<p>WordPress w wersji 4.8.1 i starsze mają następujące problemy z bezpieczeństwem:</p>\n<ol>\n<li><strong>$wpdb-&gt; prepare()</strong> mogło tworzyć nieoczekiwane i niebezpieczne zapytania prowadzące do potencjalnego wstrzyknięcia kod SQL (SQLi). Core WordPressa nie jest bezpośrednio narażony na ten problem, ale poprawiono proces tworzenia zapytań aby zapobiegać podatności we wtyczkach i motywach. Zgłoszone przez <a href="https://hackerone.com/slavco">Slavco</a></li>\n<li>Wykryto podatność na cross-site scripting (XSS) w używaniu oEmbed. Zgłoszone przez xknown zespołu ds. Zabezpieczeń WordPress.</li>\n<li>W edytorze wizualnym wykryto lukę w zabezpieczeniach skryptów przed cross-site scripting (XSS) Zgłoszone przez <a href="https://twitter.com/brutelogic">Rodolfo Assis (@brutelogic)</a> Sucuri Security.</li>\n<li>W kodzie do rozpakowywania wykryto lukę w zabezpieczeniach ścieżki do pliku. Zgłoszone przez <a href="https://hackerone.com/noxrnet">Alex Chapman (noxrnet)</a>.</li>\n<li>W edytorze wtyczek wykryto luka w zabezpieczeniach cross-site scripting (XSS). Zgłoszone przez 陈瑞琦 (Chen Ruiqi).</li>\n<li>Na ekranie użytkownika i terminach edycji zostały wykryte otwarte przekierowanie. Zgłoszone przez <a href="https://hackerone.com/ysx">Yasin Soliman (ysx)</a>.</li>\n<li>Odkryto lukę w zabezpieczeniach ścieżki na ekranie personalizacji witryny. Zgłoszone przez Weston Ruter z zespołu ds. Bezpieczeństwa WordPress.</li>\n<li>Wykryto podatność na cross-site scripting (XSS)  w nazwach szablonów. Zgłoszone przez <a href="https://hackerone.com/sikic">Luka (sikic)</a>.</li>\n<li>Wykryto podatność na cross-site scripting (XSS) w linkach modalnych. Zgłoszone przez <a href="https://hackerone.com/qasuar">Anas Roubi (qasuar)</a>.</li>\n</ol>\n<p>Dziękujemy zgłaszającym za praktykowanie <a href="https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/">odpowiedzialnego ujawnienia (en)</a>.</p>\n<p>Oprócz powyższych kwestii związanych z bezpieczeństwem, WordPress 4.8.2 zawiera 6 poprawek utrzymaniowych serii 4.8. Więcej informacji można znaleźć w <a href="https://codex.wordpress.org/Version_4.8.2">notce wydania wersji (en)</a> oraz na <a href="https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.8.2&amp;group=component&amp;col=id&amp;col=summary&amp;col=component&amp;col=status&amp;col=owner&amp;col=type&amp;col=priority&amp;col=keywords&amp;order=priority">liście zmian</a>.</p>\n<p><a href="https://pl.wordpress.org/wordpress-4.8.2-pl_PL.zip">Pobierz WordPress 4.8.2</a> lub przejdź do Panel → Aktualizacje i kliknij przycisk &#8222;Aktualizuj teraz&#8221;. Strony obsługujące automatyczne aktualizacje w tle już zaczynają aktualizować się do wersji 4.8.2.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do wydania 4.8.2.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:101:"https://pl.wordpress.org/2017/09/22/wordpress-4-8-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:42:"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:51:"Zrób jedną rzecz i zmień przyszłość WordCampa";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://pl.wordpress.org/2017/06/15/zrob-cos-dla-wordpressa/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:68:"https://pl.wordpress.org/2017/06/15/zrob-cos-dla-wordpressa/#respond";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 15 Jun 2017 04:11:28 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:10:"Wydarzenia";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://pl.wordpress.org/?p=977";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:435:"Ósmy polski WordCamp już za nami. Polska społeczność rozrasta się z roku na rok, co bardzo cieszy, ale niejednokrotnie stanowi wyzwanie dla organizatorów spotkań. Chcemy lepiej odpowiadać na potrzeby polskich twórców i użytkowników WordPressa. Dlatego przygotowaliśmy krótką ankietę na temat WordCampa. Chcemy poznać Was lepiej, dowiedzieć się, kim jesteście i jakie macie oczekiwania względem kolejnych [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:18:"Katarzyna Gajewska";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:815:"<p>Ósmy polski WordCamp już za nami. Polska społeczność rozrasta się z roku na rok, co bardzo cieszy, ale niejednokrotnie stanowi wyzwanie dla organizatorów spotkań.</p>\n<p>Chcemy lepiej odpowiadać na potrzeby polskich twórców i użytkowników WordPressa. Dlatego przygotowaliśmy krótką ankietę na temat WordCampa. Chcemy poznać Was lepiej, dowiedzieć się, kim jesteście i jakie macie oczekiwania względem kolejnych edycji WordCampa w Polsce. Wypełnienie ankiety nie zajmie Wam więcej niż 5 minut.</p>\n<p><a href="https://goo.gl/forms/VPY4xtO9KF6fuDnm1" target="_blank" rel="noopener"><strong>Wypełnij ankietę na temat polskiej społeczności WordPressowej</strong></a>.</p>\n<p>Dziękujemy Wam za wypełnienie ankiety!</p>\n<p><em>Organizatorzy polskich WordUpów i WordCamp Polska</em></p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:65:"https://pl.wordpress.org/2017/06/15/zrob-cos-dla-wordpressa/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:42:"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"WordPress 4.7.5 – Wydanie poprawiające bezpieczeństwo i błędy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:96:"https://pl.wordpress.org/2017/05/17/wordpress-4-7-5-wydanie-poprawiajace-bezpieczenstwo-i-bledy/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:105:"https://pl.wordpress.org/2017/05/17/wordpress-4-7-5-wydanie-poprawiajace-bezpieczenstwo-i-bledy/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 17 May 2017 00:14:32 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:7:"Wydanie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://pl.wordpress.org/?p=907";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:490:"WordPress 4.7.5 jest już dostępny. To wydanie poprawia bezpieczeństwo we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji. WordPress w wersji 4.7.4 i wcześniejszych posiada sześć problemów z bezpieczeństwem: Niewystarczająca weryfikacja przekierowań w klasie HTTP. Zgłosił to Ronni Skansing. Niewłaściwa obsługa wartości danych meta wpisów w API XML-RPC. Zgłosił to Sam Thomas. Brak sprawdzania uprawnień dla [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:37:"Krzysztof Trynkiewicz (Sukces Strony)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2591:"<p>WordPress 4.7.5 jest już dostępny. <strong>To wydanie poprawia bezpieczeństwo</strong> we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji.</p>\n<p><span id="more-907"></span></p>\n<p>WordPress w wersji 4.7.4 i wcześniejszych posiada sześć problemów z bezpieczeństwem:</p>\n<ol>\n<li>Niewystarczająca weryfikacja przekierowań w klasie HTTP. Zgłosił to <a href="https://dk.linkedin.com/in/ronni-skansing-36143b65">Ronni Skansing</a>.</li>\n<li>Niewłaściwa obsługa wartości danych meta wpisów w API XML-RPC. Zgłosił to <a href="https://hackerone.com/jazzy2fives">Sam Thomas</a>.</li>\n<li>Brak sprawdzania uprawnień dla danych meta wpisów w API XML-RPC. Zgłosił to <a href="https://profiles.wordpress.org/vortfu">Ben Bidner</a> z Zespołu Bezpieczeństwa WordPressa.</li>\n<li>Podatność typu Cross Site Request Forgery (CRSF) została wykryta w formularzu zapytania o dane dostępu do systemu plików. Zgłosił to <a href="https://twitter.com/yorickkoster">Yorick Koster</a>.</li>\n<li>Wykryto podatność typu cross-site scripting (XSS). Objawia się podczas próby wgrania bardzo dużych plików. Zgłosił to <a href="https://dk.linkedin.com/in/ronni-skansing-36143b65">Ronni Skansing</a>.</li>\n<li>Wykryto podatność typu cross-site scripting (XSS). Ma ona związek z mechanizmem Personalizacji witryn. Zgłosił to <a href="https://profiles.wordpress.org/westonruter">Weston Ruter</a> z Zespołu Bezpieczeństwa WordPressa.</li>\n</ol>\n<p>Dziękujemy zgłaszającym za <a href="https://make.wordpress.org/core/handbook/reporting-security-vulnerabilities/">odpowiedzialne ujawnienie</a> problemów z bezpieczeństwem.</p>\n<p>Oprócz powyższych problemów z bezpieczeństwem, WordPress 4.7.5 naprawia także 3 błędy z wersji 4.7. Więcej informacji można znaleźć w <a href="https://codex.wordpress.org/Version_4.7.5">notatkach do wydania</a> oraz <a href="https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.7.5&amp;group=component&amp;col=id&amp;col=summary&amp;col=component&amp;col=status&amp;col=owner&amp;col=type&amp;col=priority&amp;col=keywords&amp;order=priority">liście przeprowadzonych zmian</a>.</p>\n<p><a href="https://pl.wordpress.org/">Pobierz WordPressa 4.7.5</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do powstania wersji 4.7.5.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:101:"https://pl.wordpress.org/2017/05/17/wordpress-4-7-5-wydanie-poprawiajace-bezpieczenstwo-i-bledy/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"8";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:42:"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"WordPress 4.7.4 – Wydanie poprawiające bezpieczeństwo i błędy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:96:"https://pl.wordpress.org/2017/04/20/wordpress-4-7-4-wydanie-poprawiajace-bezpieczenstwo-i-bledy/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:104:"https://pl.wordpress.org/2017/04/20/wordpress-4-7-4-wydanie-poprawiajace-bezpieczenstwo-i-bledy/#respond";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 20 Apr 2017 18:42:55 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:7:"Wydanie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://pl.wordpress.org/?p=858";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:490:"Po niemal 60 milionach pobrań WordPressa 4.7 z radością ogłaszamy dostępność wersji 4.7.4 &#8211; wydania poprawiającego błędy i bezpieczeństwo. Wydanie to zawiera 47 poprawek błędów oraz usprawnień. W szczególności naprawiony został brak kompatybilności między nadchodzącą wersją przeglądarki Chrome i edytorem wizualnym, jak również poprawiono braki w spójności obsługi mediów, a także wprowadzono dalsze ulepszenia API REST. Więcej informacji [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:37:"Krzysztof Trynkiewicz (Sukces Strony)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4736:"<p>Po niemal 60 milionach pobrań WordPressa 4.7 z radością ogłaszamy dostępność wersji 4.7.4 &#8211; <strong>wydania poprawiającego błędy i bezpieczeństwo.</strong></p>\n<p><span id="more-858"></span></p>\n<p>Wydanie to zawiera 47 poprawek błędów oraz usprawnień. W szczególności naprawiony został brak kompatybilności między nadchodzącą wersją przeglądarki Chrome i edytorem wizualnym, jak również poprawiono braki w spójności obsługi mediów, a także wprowadzono dalsze ulepszenia API REST. Więcej informacji można znaleźć w <a href="https://codex.wordpress.org/Version_4.7.4">notatkach do wydania</a> oraz <a href="https://core.trac.wordpress.org/log/branches/4.7?rev=40487&amp;stop_rev=40224">liście przeprowadzonych zmian</a>.</p>\n<p><a href="https://pl.wordpress.org/">Pobierz WordPressa 4.7.4</a> lub udaj się do sekcji <strong>Kokpit → Aktualizacje</strong> panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do powstania wersji 4.7.4:<br />\n<a href="https://profiles.wordpress.org/jorbin/">Aaron Jorbin</a>, <a href="https://profiles.wordpress.org/adamsilverstein/">Adam Silverstein</a>, <a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>, <a href="https://profiles.wordpress.org/aussieguy123/">aussieguy123</a>, <a href="https://profiles.wordpress.org/blobfolio/">Blobfolio</a>, <a href="https://profiles.wordpress.org/boldwater/">boldwater</a>, <a href="https://profiles.wordpress.org/boonebgorges/">Boone Gorges</a>, <a href="https://profiles.wordpress.org/bor0/">Boro Sitnikovski</a>, <a href="https://profiles.wordpress.org/chesio/">chesio</a>, <a href="https://profiles.wordpress.org/curdin/">Curdin Krummenacher</a>, <a href="https://profiles.wordpress.org/danielbachhuber/">Daniel Bachhuber</a>, <a href="https://profiles.wordpress.org/nerrad/">Darren Ethier (nerrad)</a>, <a href="https://profiles.wordpress.org/davidakennedy/">David A. Kennedy</a>, <a href="https://profiles.wordpress.org/davidbenton/">davidbenton</a>, <a href="https://profiles.wordpress.org/dlh/">David Herrera</a>, <a href="https://profiles.wordpress.org/dd32/">Dion Hulse</a>, <a href="https://profiles.wordpress.org/ocean90/">Dominik Schilling (ocean90)</a>, <a href="https://profiles.wordpress.org/eclev91/">eclev91</a>, <a href="https://profiles.wordpress.org/iseulde/">Ella Van Dorpe</a>, <a href="https://profiles.wordpress.org/ghosttoast/">Gustave F. Gerhardt</a>, <a href="https://profiles.wordpress.org/ig_communitysites/">ig_communitysites</a>, <a href="https://profiles.wordpress.org/jnylen0/">James Nylen</a>, <a href="https://profiles.wordpress.org/joedolson/">Joe Dolson</a>, <a href="https://profiles.wordpress.org/johnbillion/">John Blackbourn</a>, <a href="https://profiles.wordpress.org/karinedo/">karinedo</a>, <a href="https://profiles.wordpress.org/lukasbesch/">lukasbesch</a>, <a href="https://profiles.wordpress.org/maguiar/">maguiar</a>, <a href="https://profiles.wordpress.org/matheusgimenez/">MatheusGimenez</a>, <a href="https://profiles.wordpress.org/mboynes/">Matthew Boynes</a>, <a href="https://profiles.wordpress.org/mattwiebe/">Matt Wiebe</a>, <a href="https://profiles.wordpress.org/mayurk/">Mayur Keshwani</a>, <a href="https://profiles.wordpress.org/melchoyce/">Mel Choyce</a>, <a href="https://profiles.wordpress.org/celloexpressions/">Nick Halsey</a>, <a href="https://profiles.wordpress.org/swissspidy/">Pascal Birchler</a>, <a href="https://profiles.wordpress.org/peterwilsoncc/">Peter Wilson</a>, <a href="https://profiles.wordpress.org/delawski/">Piotr Delawski</a>, <a href="https://profiles.wordpress.org/pratikshrestha/">Pratik Shrestha</a>, <a href="https://profiles.wordpress.org/programmin/">programmin</a>, <a href="https://profiles.wordpress.org/rachelbaker/">Rachel Baker</a>, <a href="https://profiles.wordpress.org/sagarkbhatt/">sagarkbhatt</a>, <a href="https://profiles.wordpress.org/sagarprajapati/">Sagar Prajapati</a>, <a href="https://profiles.wordpress.org/sboisvert/">sboisvert</a>, <a href="https://profiles.wordpress.org/wonderboymusic/">Scott Taylor</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, <a href="https://profiles.wordpress.org/netweb/">Stephen Edgar</a>, <a href="https://profiles.wordpress.org/cybr/">Sybre Waaijer</a>, <a href="https://profiles.wordpress.org/timmydcrawford/">Timmy Crawford</a>, <a href="https://profiles.wordpress.org/vortfu/">vortfu</a> oraz <a href="https://profiles.wordpress.org/westonruter/">Weston Ruter</a>.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:101:"https://pl.wordpress.org/2017/04/20/wordpress-4-7-4-wydanie-poprawiajace-bezpieczenstwo-i-bledy/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:30:"https://pl.wordpress.org/feed/";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:9:"\n	hourly	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:4:"\n	1	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:"\0*\0data";a:8:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Tue, 19 Jun 2018 06:49:06 GMT";s:12:"content-type";s:34:"application/rss+xml; charset=UTF-8";s:6:"x-olaf";s:3:"⛄";s:13:"last-modified";s:29:"Sat, 26 May 2018 08:28:57 GMT";s:4:"link";s:61:"<https://pl.wordpress.org/wp-json/>; rel="https://api.w.org/"";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:9:"HIT ord 2";}}s:5:"build";s:14:"20130911040210";}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(239, '_transient_timeout_feed_mod_a421d6f32723068ab074a40017a9e1f9', '1529434148', 'no'),
(240, '_transient_feed_mod_a421d6f32723068ab074a40017a9e1f9', '1529390948', 'no'),
(241, '_transient_timeout_dash_v2_5ed7e1a5d79caaf375d35c33f6782653', '1529434148', 'no'),
(242, '_transient_dash_v2_5ed7e1a5d79caaf375d35c33f6782653', '<div class="rss-widget"><ul><li><a class=''rsswidget'' href=''https://pl.wordpress.org/2018/05/26/program-wordcamp-poznan-2018-juz-tu-jest/''>Program WordCamp Poznań 2018 już tu jest!</a></li></ul></div><div class="rss-widget"><ul><li><a class=''rsswidget'' href=''https://pl.wordpress.org/2018/05/26/program-wordcamp-poznan-2018-juz-tu-jest/''>Program WordCamp Poznań 2018 już tu jest!</a></li><li><a class=''rsswidget'' href=''https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/''>WordPress 4.9.4 – Wydanie poprawiające ważny błąd</a></li><li><a class=''rsswidget'' href=''https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/''>WordPress 4.9.3 – Wydanie poprawiające błędy</a></li></ul></div>', 'no'),
(245, '_transient_timeout_plugin_slugs', '1529486153', 'no'),
(246, '_transient_plugin_slugs', 'a:3:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:19:"akismet/akismet.php";i:2;s:9:"hello.php";}', 'no');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB AUTO_INCREMENT=978 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_last', '1'),
(4, 5, '_edit_lock', '1528897663:1'),
(5, 6, '_edit_last', '1'),
(6, 6, '_edit_lock', '1529399113:1'),
(7, 6, '_wp_page_template', 'frontpage.php'),
(8, 8, '_edit_last', '1'),
(9, 8, '_edit_lock', '1529405979:1'),
(10, 10, '_menu_item_type', 'custom'),
(11, 10, '_menu_item_menu_item_parent', '0'),
(12, 10, '_menu_item_object_id', '10'),
(13, 10, '_menu_item_object', 'custom'),
(14, 10, '_menu_item_target', ''),
(15, 10, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(16, 10, '_menu_item_xfn', ''),
(17, 10, '_menu_item_url', 'http://localhost/idea2/'),
(18, 10, '_menu_item_orphaned', '1528970122'),
(19, 11, '_menu_item_type', 'post_type'),
(20, 11, '_menu_item_menu_item_parent', '0'),
(21, 11, '_menu_item_object_id', '6'),
(22, 11, '_menu_item_object', 'page'),
(23, 11, '_menu_item_target', ''),
(24, 11, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(25, 11, '_menu_item_xfn', ''),
(26, 11, '_menu_item_url', ''),
(27, 11, '_menu_item_orphaned', '1528970122'),
(28, 12, '_menu_item_type', 'post_type'),
(29, 12, '_menu_item_menu_item_parent', '0'),
(30, 12, '_menu_item_object_id', '2'),
(31, 12, '_menu_item_object', 'page'),
(32, 12, '_menu_item_target', ''),
(33, 12, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(34, 12, '_menu_item_xfn', ''),
(35, 12, '_menu_item_url', ''),
(36, 12, '_menu_item_orphaned', '1528970123'),
(37, 13, '_menu_item_type', 'custom'),
(38, 13, '_menu_item_menu_item_parent', '0'),
(39, 13, '_menu_item_object_id', '13'),
(40, 13, '_menu_item_object', 'custom'),
(41, 13, '_menu_item_target', ''),
(42, 13, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(43, 13, '_menu_item_xfn', ''),
(44, 13, '_menu_item_url', 'http://localhost/idea2/home/'),
(64, 16, '_menu_item_type', 'custom'),
(65, 16, '_menu_item_menu_item_parent', '0'),
(66, 16, '_menu_item_object_id', '16'),
(67, 16, '_menu_item_object', 'custom'),
(68, 16, '_menu_item_target', ''),
(69, 16, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(70, 16, '_menu_item_xfn', ''),
(71, 16, '_menu_item_url', 'http://localhost/idea2/projects/'),
(73, 17, '_menu_item_type', 'custom'),
(74, 17, '_menu_item_menu_item_parent', '0'),
(75, 17, '_menu_item_object_id', '17'),
(76, 17, '_menu_item_object', 'custom'),
(77, 17, '_menu_item_target', ''),
(78, 17, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(79, 17, '_menu_item_xfn', ''),
(80, 17, '_menu_item_url', 'http://localhost/idea2/contact/'),
(82, 18, '_menu_item_type', 'custom'),
(83, 18, '_menu_item_menu_item_parent', '0'),
(84, 18, '_menu_item_object_id', '18'),
(85, 18, '_menu_item_object', 'custom'),
(86, 18, '_menu_item_target', ''),
(87, 18, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(88, 18, '_menu_item_xfn', ''),
(89, 18, '_menu_item_url', 'http://localhost/idea2/about/'),
(91, 19, '_menu_item_type', 'custom'),
(92, 19, '_menu_item_menu_item_parent', '0'),
(93, 19, '_menu_item_object_id', '19'),
(94, 19, '_menu_item_object', 'custom'),
(95, 19, '_menu_item_target', ''),
(96, 19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(97, 19, '_menu_item_xfn', ''),
(98, 19, '_menu_item_url', 'http://localhost/idea2/job-offers/'),
(99, 6, 'box1', 'sdcsdcccc'),
(100, 6, '_box1', 'field_5b21239d17b89'),
(101, 20, 'box1', 'sdcsdcccc'),
(102, 20, '_box1', 'field_5b21239d17b89'),
(103, 6, 'name12', 'Text to enter'),
(104, 6, '_name12', 'field_5b237a1820b0f'),
(105, 23, 'box1', 'sdcsdcccc'),
(106, 23, '_box1', 'field_5b21239d17b89'),
(107, 23, 'name12', 'Text to enter'),
(108, 23, '_name12', 'field_5b237a1820b0f'),
(111, 6, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(112, 6, '_example12', 'field_5b237e4c4578e'),
(113, 27, 'box1', 'sdcsdcccc'),
(114, 27, '_box1', 'field_5b21239d17b89'),
(115, 27, 'name12', 'Text to enter'),
(116, 27, '_name12', 'field_5b237a1820b0f'),
(117, 27, 'example12', 'texttext'),
(118, 27, '_example12', 'field_5b237e4c4578e'),
(123, 29, 'name12', 'Text to enter'),
(124, 29, '_name12', 'field_5b237a1820b0f'),
(125, 29, 'example12', 'texttext'),
(126, 29, '_example12', 'field_5b237e4c4578e'),
(127, 29, 'box1', 'sdcsdcccc'),
(128, 29, '_box1', 'field_5b21239d17b89'),
(129, 30, 'box1', 'sdcsdcccc'),
(130, 30, '_box1', 'field_5b21239d17b89'),
(131, 30, 'name12', 'Text to enter'),
(132, 30, '_name12', 'field_5b237a1820b0f'),
(133, 30, 'example12', 'nskjcnslkjdncalksjdcn'),
(134, 30, '_example12', 'field_5b237e4c4578e'),
(135, 31, 'box1', 'sdcsdcccc'),
(136, 31, '_box1', 'field_5b21239d17b89'),
(137, 31, 'name12', 'Text to enter'),
(138, 31, '_name12', 'field_5b237a1820b0f'),
(139, 31, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(140, 31, '_example12', 'field_5b237e4c4578e'),
(141, 6, 'grey1', 'Box 1'),
(142, 6, '_grey1', 'field_5b237e4c4578e'),
(143, 6, 'grey2', 'Box 2'),
(144, 6, '_grey2', 'field_5b2387af232ca'),
(145, 6, 'grey3', 'Box 3'),
(146, 6, '_grey3', 'field_5b2387bc232cb'),
(147, 6, 'grey4', 'Box 4'),
(148, 6, '_grey4', 'field_5b2387c4232cc'),
(149, 6, 'grey5', 'Box 5'),
(150, 6, '_grey5', 'field_5b2387ce232cd'),
(151, 6, 'photoleft', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(152, 6, '_photoleft', 'field_5b23883faea3e'),
(153, 6, 'photoright', 'Excepturi sint occaecati cupiditate accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(154, 6, '_photoright', 'field_5b23886faea3f'),
(155, 38, 'box1', 'sdcsdcccc'),
(156, 38, '_box1', 'field_5b21239d17b89'),
(157, 38, 'name12', 'Text to enter'),
(158, 38, '_name12', 'field_5b237a1820b0f'),
(159, 38, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(160, 38, '_example12', 'field_5b237e4c4578e'),
(161, 38, 'grey1', 'Box 1'),
(162, 38, '_grey1', 'field_5b237e4c4578e'),
(163, 38, 'grey2', 'Box 2'),
(164, 38, '_grey2', 'field_5b2387af232ca'),
(165, 38, 'grey3', 'Box 3'),
(166, 38, '_grey3', 'field_5b2387bc232cb'),
(167, 38, 'grey4', 'Box 4'),
(168, 38, '_grey4', 'field_5b2387c4232cc'),
(169, 38, 'grey5', 'Box 5'),
(170, 38, '_grey5', 'field_5b2387ce232cd'),
(171, 38, 'photoleft', ''),
(172, 38, '_photoleft', 'field_5b23883faea3e'),
(173, 38, 'photoright', ''),
(174, 38, '_photoright', 'field_5b23886faea3f'),
(175, 39, 'box1', 'sdcsdcccc'),
(176, 39, '_box1', 'field_5b21239d17b89'),
(177, 39, 'name12', 'Text to enter'),
(178, 39, '_name12', 'field_5b237a1820b0f'),
(179, 39, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(180, 39, '_example12', 'field_5b237e4c4578e'),
(181, 39, 'grey1', 'Box 1'),
(182, 39, '_grey1', 'field_5b237e4c4578e'),
(183, 39, 'grey2', 'Box 2'),
(184, 39, '_grey2', 'field_5b2387af232ca'),
(185, 39, 'grey3', 'Box 3'),
(186, 39, '_grey3', 'field_5b2387bc232cb'),
(187, 39, 'grey4', 'Box 4'),
(188, 39, '_grey4', 'field_5b2387c4232cc'),
(189, 39, 'grey5', 'Box 5'),
(190, 39, '_grey5', 'field_5b2387ce232cd'),
(191, 39, 'photoleft', 'Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 '),
(192, 39, '_photoleft', 'field_5b23883faea3e'),
(193, 39, 'photoright', 'Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 '),
(194, 39, '_photoright', 'field_5b23886faea3f'),
(195, 40, 'box1', 'sdcsdcccc'),
(196, 40, '_box1', 'field_5b21239d17b89'),
(197, 40, 'name12', 'Text to enter'),
(198, 40, '_name12', 'field_5b237a1820b0f'),
(199, 40, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(200, 40, '_example12', 'field_5b237e4c4578e'),
(201, 40, 'grey1', 'Box 1'),
(202, 40, '_grey1', 'field_5b237e4c4578e'),
(203, 40, 'grey2', 'Box 2'),
(204, 40, '_grey2', 'field_5b2387af232ca'),
(205, 40, 'grey3', 'Box 3'),
(206, 40, '_grey3', 'field_5b2387bc232cb'),
(207, 40, 'grey4', 'Box 4'),
(208, 40, '_grey4', 'field_5b2387c4232cc'),
(209, 40, 'grey5', 'Box 5'),
(210, 40, '_grey5', 'field_5b2387ce232cd'),
(211, 40, 'photoleft', 'Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 '),
(212, 40, '_photoleft', 'field_5b23883faea3e'),
(213, 40, 'photoright', 'Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2'),
(214, 40, '_photoright', 'field_5b23886faea3f'),
(215, 41, '_edit_last', '1'),
(216, 41, '_edit_lock', '1529398295:1'),
(217, 41, '_wp_page_template', 'about.php'),
(218, 43, '_edit_last', '1'),
(219, 43, '_edit_lock', '1529064910:1'),
(220, 41, 'abouttextleft', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.'),
(221, 41, '_abouttextleft', 'field_5b238e9d9c51a'),
(222, 41, 'abouttextright', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'),
(223, 41, '_abouttextright', 'field_5b238ece9c51b'),
(224, 46, 'abouttextleft', 'About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left '),
(225, 46, '_abouttextleft', 'field_5b238e9d9c51a'),
(226, 46, 'abouttextright', 'About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right '),
(227, 46, '_abouttextright', 'field_5b238ece9c51b'),
(228, 41, 'quote', 'Two things are infinite: the universe and human stupidity; and I''m not sure about the universe'),
(229, 41, '_quote', 'field_5b239037b29b4'),
(230, 41, 'quoteauthor', 'Albert Einstein'),
(231, 41, '_quoteauthor', 'field_5b23906ed79be'),
(232, 49, 'abouttextleft', 'About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left '),
(233, 49, '_abouttextleft', 'field_5b238e9d9c51a'),
(234, 49, 'abouttextright', 'About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right '),
(235, 49, '_abouttextright', 'field_5b238ece9c51b'),
(236, 49, 'quote', 'Two things are infinite: the universe and human stupidity; and I''m not sure about the universe'),
(237, 49, '_quote', 'field_5b239037b29b4'),
(238, 49, 'quoteauthor', 'Albert Einstein'),
(239, 49, '_quoteauthor', 'field_5b23906ed79be'),
(240, 41, 'textonimage-name', 'John Smith'),
(241, 41, '_textonimage-name', 'field_5b23915430599'),
(242, 41, 'textonimage', 'WordPress is software designed for everyone, emphasizing accessibility, performance, security, and ease of use. We believe great software should work with minimum set up, so you can focus on sharing your story, product, or services freely.'),
(243, 41, '_textonimage', 'field_5b23918d3059a'),
(244, 52, 'abouttextleft', 'About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left '),
(245, 52, '_abouttextleft', 'field_5b238e9d9c51a'),
(246, 52, 'abouttextright', 'About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right '),
(247, 52, '_abouttextright', 'field_5b238ece9c51b'),
(248, 52, 'quote', 'Two things are infinite: the universe and human stupidity; and I''m not sure about the universe'),
(249, 52, '_quote', 'field_5b239037b29b4'),
(250, 52, 'quoteauthor', 'Albert Einstein'),
(251, 52, '_quoteauthor', 'field_5b23906ed79be'),
(252, 52, 'textonimage-name', 'John Smith'),
(253, 52, '_textonimage-name', 'field_5b23915430599'),
(254, 52, 'textonimage', 'WordPress is software designed for everyone, emphasizing accessibility, performance, security, and ease of use.'),
(255, 52, '_textonimage', 'field_5b23918d3059a'),
(256, 53, 'abouttextleft', 'About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left About text left '),
(257, 53, '_abouttextleft', 'field_5b238e9d9c51a'),
(258, 53, 'abouttextright', 'About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right About text right '),
(259, 53, '_abouttextright', 'field_5b238ece9c51b'),
(260, 53, 'quote', 'Two things are infinite: the universe and human stupidity; and I''m not sure about the universe'),
(261, 53, '_quote', 'field_5b239037b29b4'),
(262, 53, 'quoteauthor', 'Albert Einstein'),
(263, 53, '_quoteauthor', 'field_5b23906ed79be'),
(264, 53, 'textonimage-name', 'John Smith'),
(265, 53, '_textonimage-name', 'field_5b23915430599'),
(266, 53, 'textonimage', 'WordPress is software designed for everyone, emphasizing accessibility, performance, security, and ease of use. We believe great software should work with minimum set up, so you can focus on sharing your story, product, or services freely.'),
(267, 53, '_textonimage', 'field_5b23918d3059a'),
(268, 54, '_edit_last', '1'),
(269, 54, '_edit_lock', '1529305048:1'),
(270, 54, '_wp_page_template', 'joboffers.php'),
(271, 56, '_edit_last', '1'),
(272, 56, '_edit_lock', '1529065640:1'),
(273, 54, 'job-name', '4'),
(274, 54, '_job-name', 'field_5b2398c9eb260'),
(275, 54, 'job-description', 'lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript '),
(276, 54, '_job-description', 'field_5b239afaeb261'),
(277, 59, 'job-name', 'Front End Developer'),
(278, 59, '_job-name', 'field_5b2398c9eb260'),
(279, 59, 'job-description', 'lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript '),
(280, 59, '_job-description', 'field_5b239afaeb261'),
(281, 54, 'job-name_0_example', 'sdcsdca'),
(282, 54, '_job-name_0_example', 'field_5b239b78d22fe'),
(283, 54, 'job-name_1_example', 'sdcasdca'),
(284, 54, '_job-name_1_example', 'field_5b239b78d22fe'),
(285, 54, 'job-name_2_example', 'sdcasdcdcdc'),
(286, 54, '_job-name_2_example', 'field_5b239b78d22fe'),
(287, 54, 'job-name_3_example', 'dfvfgb'),
(288, 54, '_job-name_3_example', 'field_5b239b78d22fe'),
(289, 61, 'job-name', '4'),
(290, 61, '_job-name', 'field_5b2398c9eb260'),
(291, 61, 'job-description', 'lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript '),
(292, 61, '_job-description', 'field_5b239afaeb261'),
(293, 61, 'job-name_0_example', 'sdcsdca'),
(294, 61, '_job-name_0_example', 'field_5b239b78d22fe'),
(295, 61, 'job-name_1_example', 'sdcasdca'),
(296, 61, '_job-name_1_example', 'field_5b239b78d22fe'),
(297, 61, 'job-name_2_example', 'sdcasdcdcdc'),
(298, 61, '_job-name_2_example', 'field_5b239b78d22fe'),
(299, 61, 'job-name_3_example', 'dfvfgb'),
(300, 61, '_job-name_3_example', 'field_5b239b78d22fe'),
(301, 54, 'job_offer_0_job_name', 'Software Engineer'),
(302, 54, '_job_offer_0_job_name', 'field_5b239cd768545'),
(303, 54, 'job_offer_0_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(304, 54, '_job_offer_0_job_description', 'field_5b239ce968546'),
(305, 54, 'job_offer_1_job_name', 'Social Media Manager'),
(306, 54, '_job_offer_1_job_name', 'field_5b239cd768545'),
(307, 54, 'job_offer_1_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(308, 54, '_job_offer_1_job_description', 'field_5b239ce968546'),
(309, 54, 'job_offer', '8'),
(310, 54, '_job_offer', 'field_5b239cbd68544'),
(311, 65, 'job-name', '4'),
(312, 65, '_job-name', 'field_5b2398c9eb260'),
(313, 65, 'job-description', 'lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript '),
(314, 65, '_job-description', 'field_5b239afaeb261'),
(315, 65, 'job-name_0_example', 'sdcsdca'),
(316, 65, '_job-name_0_example', 'field_5b239b78d22fe'),
(317, 65, 'job-name_1_example', 'sdcasdca'),
(318, 65, '_job-name_1_example', 'field_5b239b78d22fe'),
(319, 65, 'job-name_2_example', 'sdcasdcdcdc'),
(320, 65, '_job-name_2_example', 'field_5b239b78d22fe'),
(321, 65, 'job-name_3_example', 'dfvfgb'),
(322, 65, '_job-name_3_example', 'field_5b239b78d22fe'),
(323, 65, 'job_offer_0_job_name', 'Front End'),
(324, 65, '_job_offer_0_job_name', 'field_5b239cd768545'),
(325, 65, 'job_offer_0_job_description', 'lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem '),
(326, 65, '_job_offer_0_job_description', 'field_5b239ce968546'),
(327, 65, 'job_offer_1_job_name', 'Front End 2'),
(328, 65, '_job_offer_1_job_name', 'field_5b239cd768545'),
(329, 65, 'job_offer_1_job_description', 'lorem lorem lorem lorem loreadfvadfvm lorem lorem lorem lorem lorem lorem loadfadfcasdv lorem lorem lore'),
(330, 65, '_job_offer_1_job_description', 'field_5b239ce968546'),
(331, 65, 'job_offer', '2'),
(332, 65, '_job_offer', 'field_5b239cbd68544'),
(333, 54, 'job_offer_0_job_link', '6'),
(334, 54, '_job_offer_0_job_link', 'field_5b239ec2ea85f'),
(335, 54, 'job_offer_1_job_link', '6'),
(336, 54, '_job_offer_1_job_link', 'field_5b239ec2ea85f'),
(337, 67, 'job-name', '4'),
(338, 67, '_job-name', 'field_5b2398c9eb260'),
(339, 67, 'job-description', 'lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript '),
(340, 67, '_job-description', 'field_5b239afaeb261'),
(341, 67, 'job-name_0_example', 'sdcsdca'),
(342, 67, '_job-name_0_example', 'field_5b239b78d22fe'),
(343, 67, 'job-name_1_example', 'sdcasdca'),
(344, 67, '_job-name_1_example', 'field_5b239b78d22fe'),
(345, 67, 'job-name_2_example', 'sdcasdcdcdc'),
(346, 67, '_job-name_2_example', 'field_5b239b78d22fe'),
(347, 67, 'job-name_3_example', 'dfvfgb'),
(348, 67, '_job-name_3_example', 'field_5b239b78d22fe'),
(349, 67, 'job_offer_0_job_name', 'Front End'),
(350, 67, '_job_offer_0_job_name', 'field_5b239cd768545'),
(351, 67, 'job_offer_0_job_description', 'lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem '),
(352, 67, '_job_offer_0_job_description', 'field_5b239ce968546'),
(353, 67, 'job_offer_1_job_name', 'Front End 2'),
(354, 67, '_job_offer_1_job_name', 'field_5b239cd768545'),
(355, 67, 'job_offer_1_job_description', 'lorem lorem lorem lorem loreadfvadfvm lorem lorem lorem lorem lorem lorem loadfadfcasdv lorem lorem lore'),
(356, 67, '_job_offer_1_job_description', 'field_5b239ce968546'),
(357, 67, 'job_offer', '2'),
(358, 67, '_job_offer', 'field_5b239cbd68544'),
(359, 67, 'job_offer_0_job_link', '6'),
(360, 67, '_job_offer_0_job_link', 'field_5b239ec2ea85f'),
(361, 67, 'job_offer_1_job_link', '6'),
(362, 67, '_job_offer_1_job_link', 'field_5b239ec2ea85f'),
(363, 68, 'job-name', '4'),
(364, 68, '_job-name', 'field_5b2398c9eb260'),
(365, 68, 'job-description', 'lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript '),
(366, 68, '_job-description', 'field_5b239afaeb261'),
(367, 68, 'job-name_0_example', 'sdcsdca'),
(368, 68, '_job-name_0_example', 'field_5b239b78d22fe'),
(369, 68, 'job-name_1_example', 'sdcasdca'),
(370, 68, '_job-name_1_example', 'field_5b239b78d22fe'),
(371, 68, 'job-name_2_example', 'sdcasdcdcdc'),
(372, 68, '_job-name_2_example', 'field_5b239b78d22fe'),
(373, 68, 'job-name_3_example', 'dfvfgb'),
(374, 68, '_job-name_3_example', 'field_5b239b78d22fe'),
(375, 68, 'job_offer_0_job_name', 'Front End'),
(376, 68, '_job_offer_0_job_name', 'field_5b239cd768545'),
(377, 68, 'job_offer_0_job_description', 'lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem '),
(378, 68, '_job_offer_0_job_description', 'field_5b239ce968546'),
(379, 68, 'job_offer_1_job_name', 'Front End 2'),
(380, 68, '_job_offer_1_job_name', 'field_5b239cd768545'),
(381, 68, 'job_offer_1_job_description', 'lorem lorem lorem lorem loreadfvadfvm lorem lorem lorem lorem lorem lorem loadfadfcasdv lorem lorem lore'),
(382, 68, '_job_offer_1_job_description', 'field_5b239ce968546'),
(383, 68, 'job_offer', '2'),
(384, 68, '_job_offer', 'field_5b239cbd68544'),
(385, 68, 'job_offer_0_job_link', '6'),
(386, 68, '_job_offer_0_job_link', 'field_5b239ec2ea85f'),
(387, 68, 'job_offer_1_job_link', '6'),
(388, 68, '_job_offer_1_job_link', 'field_5b239ec2ea85f'),
(389, 54, 'job_offer_2_job_name', 'Consultant'),
(390, 54, '_job_offer_2_job_name', 'field_5b239cd768545'),
(391, 54, 'job_offer_2_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(392, 54, '_job_offer_2_job_description', 'field_5b239ce968546'),
(393, 54, 'job_offer_2_job_link', '6'),
(394, 54, '_job_offer_2_job_link', 'field_5b239ec2ea85f'),
(395, 54, 'job_offer_3_job_name', 'Energy Advisor'),
(396, 54, '_job_offer_3_job_name', 'field_5b239cd768545'),
(397, 54, 'job_offer_3_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(398, 54, '_job_offer_3_job_description', 'field_5b239ce968546'),
(399, 54, 'job_offer_3_job_link', '6'),
(400, 54, '_job_offer_3_job_link', 'field_5b239ec2ea85f'),
(401, 69, 'job-name', '4'),
(402, 69, '_job-name', 'field_5b2398c9eb260'),
(403, 69, 'job-description', 'lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript '),
(404, 69, '_job-description', 'field_5b239afaeb261'),
(405, 69, 'job-name_0_example', 'sdcsdca'),
(406, 69, '_job-name_0_example', 'field_5b239b78d22fe'),
(407, 69, 'job-name_1_example', 'sdcasdca'),
(408, 69, '_job-name_1_example', 'field_5b239b78d22fe'),
(409, 69, 'job-name_2_example', 'sdcasdcdcdc'),
(410, 69, '_job-name_2_example', 'field_5b239b78d22fe'),
(411, 69, 'job-name_3_example', 'dfvfgb'),
(412, 69, '_job-name_3_example', 'field_5b239b78d22fe'),
(413, 69, 'job_offer_0_job_name', 'Front End'),
(414, 69, '_job_offer_0_job_name', 'field_5b239cd768545'),
(415, 69, 'job_offer_0_job_description', 'lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem '),
(416, 69, '_job_offer_0_job_description', 'field_5b239ce968546'),
(417, 69, 'job_offer_1_job_name', 'Front End 2'),
(418, 69, '_job_offer_1_job_name', 'field_5b239cd768545'),
(419, 69, 'job_offer_1_job_description', 'lorem lorem lorem lorem loreadfvadfvm lorem lorem lorem lorem lorem lorem loadfadfcasdv lorem lorem lore'),
(420, 69, '_job_offer_1_job_description', 'field_5b239ce968546'),
(421, 69, 'job_offer', '4'),
(422, 69, '_job_offer', 'field_5b239cbd68544'),
(423, 69, 'job_offer_0_job_link', '6'),
(424, 69, '_job_offer_0_job_link', 'field_5b239ec2ea85f'),
(425, 69, 'job_offer_1_job_link', '6'),
(426, 69, '_job_offer_1_job_link', 'field_5b239ec2ea85f'),
(427, 69, 'job_offer_2_job_name', 'Software Developer'),
(428, 69, '_job_offer_2_job_name', 'field_5b239cd768545'),
(429, 69, 'job_offer_2_job_description', 'adfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadf'),
(430, 69, '_job_offer_2_job_description', 'field_5b239ce968546'),
(431, 69, 'job_offer_2_job_link', '6'),
(432, 69, '_job_offer_2_job_link', 'field_5b239ec2ea85f'),
(433, 69, 'job_offer_3_job_name', 'Sushi Maker'),
(434, 69, '_job_offer_3_job_name', 'field_5b239cd768545'),
(435, 69, 'job_offer_3_job_description', 'adfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadfadf'),
(436, 69, '_job_offer_3_job_description', 'field_5b239ce968546'),
(437, 69, 'job_offer_3_job_link', '6'),
(438, 69, '_job_offer_3_job_link', 'field_5b239ec2ea85f'),
(439, 54, 'job_offer_4_job_name', 'Intern'),
(440, 54, '_job_offer_4_job_name', 'field_5b239cd768545'),
(441, 54, 'job_offer_4_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(442, 54, '_job_offer_4_job_description', 'field_5b239ce968546'),
(443, 54, 'job_offer_4_job_link', '6'),
(444, 54, '_job_offer_4_job_link', 'field_5b239ec2ea85f'),
(445, 54, 'job_offer_5_job_name', 'Research Assistant'),
(446, 54, '_job_offer_5_job_name', 'field_5b239cd768545'),
(447, 54, 'job_offer_5_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(448, 54, '_job_offer_5_job_description', 'field_5b239ce968546'),
(449, 54, 'job_offer_5_job_link', '6'),
(450, 54, '_job_offer_5_job_link', 'field_5b239ec2ea85f'),
(451, 54, 'job_offer_6_job_name', 'Managing Director'),
(452, 54, '_job_offer_6_job_name', 'field_5b239cd768545'),
(453, 54, 'job_offer_6_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(454, 54, '_job_offer_6_job_description', 'field_5b239ce968546'),
(455, 54, 'job_offer_6_job_link', '6'),
(456, 54, '_job_offer_6_job_link', 'field_5b239ec2ea85f'),
(457, 54, 'job_offer_7_job_name', 'Data Analyst'),
(458, 54, '_job_offer_7_job_name', 'field_5b239cd768545'),
(459, 54, 'job_offer_7_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(460, 54, '_job_offer_7_job_description', 'field_5b239ce968546'),
(461, 54, 'job_offer_7_job_link', '6'),
(462, 54, '_job_offer_7_job_link', 'field_5b239ec2ea85f'),
(463, 70, 'job-name', '4'),
(464, 70, '_job-name', 'field_5b2398c9eb260'),
(465, 70, 'job-description', 'lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript lorem javascript '),
(466, 70, '_job-description', 'field_5b239afaeb261'),
(467, 70, 'job-name_0_example', 'sdcsdca'),
(468, 70, '_job-name_0_example', 'field_5b239b78d22fe'),
(469, 70, 'job-name_1_example', 'sdcasdca'),
(470, 70, '_job-name_1_example', 'field_5b239b78d22fe'),
(471, 70, 'job-name_2_example', 'sdcasdcdcdc'),
(472, 70, '_job-name_2_example', 'field_5b239b78d22fe'),
(473, 70, 'job-name_3_example', 'dfvfgb'),
(474, 70, '_job-name_3_example', 'field_5b239b78d22fe'),
(475, 70, 'job_offer_0_job_name', 'Software Engineer'),
(476, 70, '_job_offer_0_job_name', 'field_5b239cd768545'),
(477, 70, 'job_offer_0_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(478, 70, '_job_offer_0_job_description', 'field_5b239ce968546'),
(479, 70, 'job_offer_1_job_name', 'Social Media Manager'),
(480, 70, '_job_offer_1_job_name', 'field_5b239cd768545'),
(481, 70, 'job_offer_1_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(482, 70, '_job_offer_1_job_description', 'field_5b239ce968546'),
(483, 70, 'job_offer', '8'),
(484, 70, '_job_offer', 'field_5b239cbd68544'),
(485, 70, 'job_offer_0_job_link', '6'),
(486, 70, '_job_offer_0_job_link', 'field_5b239ec2ea85f'),
(487, 70, 'job_offer_1_job_link', '6'),
(488, 70, '_job_offer_1_job_link', 'field_5b239ec2ea85f'),
(489, 70, 'job_offer_2_job_name', 'Consultant'),
(490, 70, '_job_offer_2_job_name', 'field_5b239cd768545'),
(491, 70, 'job_offer_2_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(492, 70, '_job_offer_2_job_description', 'field_5b239ce968546'),
(493, 70, 'job_offer_2_job_link', '6'),
(494, 70, '_job_offer_2_job_link', 'field_5b239ec2ea85f'),
(495, 70, 'job_offer_3_job_name', 'Energy Advisor'),
(496, 70, '_job_offer_3_job_name', 'field_5b239cd768545'),
(497, 70, 'job_offer_3_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(498, 70, '_job_offer_3_job_description', 'field_5b239ce968546'),
(499, 70, 'job_offer_3_job_link', '6'),
(500, 70, '_job_offer_3_job_link', 'field_5b239ec2ea85f'),
(501, 70, 'job_offer_4_job_name', 'Intern'),
(502, 70, '_job_offer_4_job_name', 'field_5b239cd768545'),
(503, 70, 'job_offer_4_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(504, 70, '_job_offer_4_job_description', 'field_5b239ce968546'),
(505, 70, 'job_offer_4_job_link', '6'),
(506, 70, '_job_offer_4_job_link', 'field_5b239ec2ea85f'),
(507, 70, 'job_offer_5_job_name', 'Research Assistant'),
(508, 70, '_job_offer_5_job_name', 'field_5b239cd768545'),
(509, 70, 'job_offer_5_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(510, 70, '_job_offer_5_job_description', 'field_5b239ce968546'),
(511, 70, 'job_offer_5_job_link', '6'),
(512, 70, '_job_offer_5_job_link', 'field_5b239ec2ea85f'),
(513, 70, 'job_offer_6_job_name', 'Managing Director'),
(514, 70, '_job_offer_6_job_name', 'field_5b239cd768545'),
(515, 70, 'job_offer_6_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(516, 70, '_job_offer_6_job_description', 'field_5b239ce968546'),
(517, 70, 'job_offer_6_job_link', '6'),
(518, 70, '_job_offer_6_job_link', 'field_5b239ec2ea85f'),
(519, 70, 'job_offer_7_job_name', 'Data Analyst'),
(520, 70, '_job_offer_7_job_name', 'field_5b239cd768545'),
(521, 70, 'job_offer_7_job_description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia, doloremque veniam. Similique, tempora facilis nulla delectus et magni voluptates error, alias deserunt, iusto obcaecati perspiciatis. Facilis dolores, cumque explicabo.'),
(522, 70, '_job_offer_7_job_description', 'field_5b239ce968546'),
(523, 70, 'job_offer_7_job_link', '6'),
(524, 70, '_job_offer_7_job_link', 'field_5b239ec2ea85f'),
(525, 5, '_wp_trash_meta_status', 'draft'),
(526, 5, '_wp_trash_meta_time', '1529064996'),
(527, 5, '_wp_desired_post_slug', ''),
(528, 73, '_edit_last', '1'),
(529, 73, '_edit_lock', '1529326537:1'),
(530, 73, '_wp_page_template', 'projects.php'),
(531, 3, '_wp_trash_meta_status', 'draft'),
(532, 3, '_wp_trash_meta_time', '1529303125'),
(533, 3, '_wp_desired_post_slug', 'polityka-prywatnosci'),
(534, 2, '_edit_lock', '1529302995:1'),
(535, 2, '_wp_trash_meta_status', 'publish'),
(536, 2, '_wp_trash_meta_time', '1529303141'),
(537, 2, '_wp_desired_post_slug', 'przykladowa-strona'),
(538, 77, '_edit_last', '1'),
(539, 77, '_edit_lock', '1529393806:1'),
(540, 85, '_wp_attached_file', '2018/06/1_27.jpg'),
(541, 85, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:387;s:6:"height";i:199;s:4:"file";s:16:"2018/06/1_27.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"1_27-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"1_27-300x154.jpg";s:5:"width";i:300;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(542, 86, '_wp_attached_file', '2018/06/1_40.jpg'),
(543, 86, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:479;s:6:"height";i:252;s:4:"file";s:16:"2018/06/1_40.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"1_40-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"1_40-300x158.jpg";s:5:"width";i:300;s:6:"height";i:158;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(544, 73, 'project_template_0_project1_image', '85'),
(545, 73, '_project_template_0_project1_image', 'field_5b275915fe881'),
(546, 73, 'project_template_0_project1_name', 'Project One'),
(547, 73, '_project_template_0_project1_name', 'field_5b27594afe882'),
(548, 73, 'project_template_0_project1_description', 'Contemporary business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(549, 73, '_project_template_0_project1_description', 'field_5b27597dfe883'),
(550, 73, 'project_template_0_project2_image', '86'),
(551, 73, '_project_template_0_project2_image', 'field_5b2759b0fe884'),
(552, 73, 'project_template_0_project2_name', 'Project Two'),
(553, 73, '_project_template_0_project2_name', 'field_5b2759d7fe885'),
(554, 73, 'project_template_0_project2_description', 'Project Lorem business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(555, 73, '_project_template_0_project2_description', 'field_5b2759eefe886'),
(556, 73, 'project_template', '4'),
(557, 73, '_project_template', 'field_5b2758bffe880'),
(558, 87, 'project_template_0_project1_image', '85'),
(559, 87, '_project_template_0_project1_image', 'field_5b275915fe881'),
(560, 87, 'project_template_0_project1_name', 'Project One'),
(561, 87, '_project_template_0_project1_name', 'field_5b27594afe882'),
(562, 87, 'project_template_0_project1_description', 'Contemporary business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(563, 87, '_project_template_0_project1_description', 'field_5b27597dfe883'),
(564, 87, 'project_template_0_project2_image', '86'),
(565, 87, '_project_template_0_project2_image', 'field_5b2759b0fe884'),
(566, 87, 'project_template_0_project2_name', 'Project Two'),
(567, 87, '_project_template_0_project2_name', 'field_5b2759d7fe885'),
(568, 87, 'project_template_0_project2_description', 'Project Lorem business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(569, 87, '_project_template_0_project2_description', 'field_5b2759eefe886'),
(570, 87, 'project_template', '1'),
(571, 87, '_project_template', 'field_5b2758bffe880'),
(572, 73, 'project_template_1_project1_image', '86'),
(573, 73, '_project_template_1_project1_image', 'field_5b275915fe881'),
(574, 73, 'project_template_1_project1_name', 'Project Two'),
(575, 73, '_project_template_1_project1_name', 'field_5b27594afe882'),
(576, 73, 'project_template_1_project1_description', 'Lorem1 business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(577, 73, '_project_template_1_project1_description', 'field_5b27597dfe883'),
(578, 73, 'project_template_2_project1_image', '85'),
(579, 73, '_project_template_2_project1_image', 'field_5b275915fe881'),
(580, 73, 'project_template_2_project1_name', 'Project Three'),
(581, 73, '_project_template_2_project1_name', 'field_5b27594afe882'),
(582, 73, 'project_template_2_project1_description', 'Lorem2 business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(583, 73, '_project_template_2_project1_description', 'field_5b27597dfe883'),
(584, 73, 'project_template_3_project1_image', '86'),
(585, 73, '_project_template_3_project1_image', 'field_5b275915fe881'),
(586, 73, 'project_template_3_project1_name', 'Project Four'),
(587, 73, '_project_template_3_project1_name', 'field_5b27594afe882'),
(588, 73, 'project_template_3_project1_description', 'Project business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(589, 73, '_project_template_3_project1_description', 'field_5b27597dfe883'),
(590, 88, 'project_template_0_project1_image', '85'),
(591, 88, '_project_template_0_project1_image', 'field_5b275915fe881'),
(592, 88, 'project_template_0_project1_name', 'Project One'),
(593, 88, '_project_template_0_project1_name', 'field_5b27594afe882'),
(594, 88, 'project_template_0_project1_description', 'Contemporary business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(595, 88, '_project_template_0_project1_description', 'field_5b27597dfe883'),
(596, 88, 'project_template_0_project2_image', '86'),
(597, 88, '_project_template_0_project2_image', 'field_5b2759b0fe884'),
(598, 88, 'project_template_0_project2_name', 'Project Two'),
(599, 88, '_project_template_0_project2_name', 'field_5b2759d7fe885'),
(600, 88, 'project_template_0_project2_description', 'Project Lorem business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(601, 88, '_project_template_0_project2_description', 'field_5b2759eefe886'),
(602, 88, 'project_template', '4'),
(603, 88, '_project_template', 'field_5b2758bffe880'),
(604, 88, 'project_template_1_project1_image', '86'),
(605, 88, '_project_template_1_project1_image', 'field_5b275915fe881'),
(606, 88, 'project_template_1_project1_name', 'Project Two'),
(607, 88, '_project_template_1_project1_name', 'field_5b27594afe882'),
(608, 88, 'project_template_1_project1_description', 'Lorem1 business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(609, 88, '_project_template_1_project1_description', 'field_5b27597dfe883'),
(610, 88, 'project_template_2_project1_image', '85'),
(611, 88, '_project_template_2_project1_image', 'field_5b275915fe881'),
(612, 88, 'project_template_2_project1_name', 'Project Three'),
(613, 88, '_project_template_2_project1_name', 'field_5b27594afe882'),
(614, 88, 'project_template_2_project1_description', 'Lorem2 business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(615, 88, '_project_template_2_project1_description', 'field_5b27597dfe883'),
(616, 88, 'project_template_3_project1_image', '86'),
(617, 88, '_project_template_3_project1_image', 'field_5b275915fe881'),
(618, 88, 'project_template_3_project1_name', 'Project Four'),
(619, 88, '_project_template_3_project1_name', 'field_5b27594afe882'),
(620, 88, 'project_template_3_project1_description', 'Project business and science treat as a project any undertaking, carried out individually or collaboratively and possibly involving research or design, that is carefully planned.'),
(621, 88, '_project_template_3_project1_description', 'field_5b27597dfe883'),
(622, 89, '_edit_last', '1'),
(623, 89, '_wp_page_template', 'contact.php'),
(624, 89, '_edit_lock', '1529329468:1'),
(625, 91, '_edit_last', '1'),
(626, 91, '_edit_lock', '1529326423:1'),
(627, 89, 'contact_text', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.'),
(628, 89, '_contact_text', 'field_5b277e0393b3d'),
(629, 93, 'contact_text', 'Textbelowemail Textbelowemail Textbelowemail Textbelowemail Textbelowemail Textbelowemail Textbelowemail '),
(630, 93, '_contact_text', 'field_5b277e0393b3d'),
(631, 94, 'contact_text', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.'),
(632, 94, '_contact_text', 'field_5b277e0393b3d'),
(633, 6, 'slide1', 'First Slide'),
(634, 6, '_slide1', 'field_5b28a8aed3e75'),
(635, 6, 'slide2', 'Second Slide'),
(636, 6, '_slide2', 'field_5b28a8c6d3e76'),
(637, 6, 'slide3', 'Third Slide'),
(638, 6, '_slide3', 'field_5b28a8dbd3e77'),
(639, 6, 'slide4', 'Fourth Slide'),
(640, 6, '_slide4', 'field_5b28a8e8d3e78'),
(641, 6, 'slide5', 'Fifth Slide'),
(642, 6, '_slide5', 'field_5b28a8f3d3e79'),
(643, 100, 'box1', 'sdcsdcccc'),
(644, 100, '_box1', 'field_5b21239d17b89'),
(645, 100, 'name12', 'Text to enter'),
(646, 100, '_name12', 'field_5b237a1820b0f'),
(647, 100, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(648, 100, '_example12', 'field_5b237e4c4578e'),
(649, 100, 'grey1', 'Box 1'),
(650, 100, '_grey1', 'field_5b237e4c4578e'),
(651, 100, 'grey2', 'Box 2'),
(652, 100, '_grey2', 'field_5b2387af232ca'),
(653, 100, 'grey3', 'Box 3'),
(654, 100, '_grey3', 'field_5b2387bc232cb'),
(655, 100, 'grey4', 'Box 4'),
(656, 100, '_grey4', 'field_5b2387c4232cc'),
(657, 100, 'grey5', 'Box 5'),
(658, 100, '_grey5', 'field_5b2387ce232cd'),
(659, 100, 'photoleft', 'Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 Lorem ipsum1 '),
(660, 100, '_photoleft', 'field_5b23883faea3e'),
(661, 100, 'photoright', 'Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2 Lorem ipsum2'),
(662, 100, '_photoright', 'field_5b23886faea3f'),
(663, 100, 'slide1', 'First Slide'),
(664, 100, '_slide1', 'field_5b28a8aed3e75'),
(665, 100, 'slide2', 'Second Slide'),
(666, 100, '_slide2', 'field_5b28a8c6d3e76'),
(667, 100, 'slide3', 'Third Slide'),
(668, 100, '_slide3', 'field_5b28a8dbd3e77'),
(669, 100, 'slide4', 'Fourth Slide'),
(670, 100, '_slide4', 'field_5b28a8e8d3e78'),
(671, 100, 'slide5', 'Fifth Slide'),
(672, 100, '_slide5', 'field_5b28a8f3d3e79'),
(673, 101, 'box1', 'sdcsdcccc'),
(674, 101, '_box1', 'field_5b21239d17b89'),
(675, 101, 'name12', 'Text to enter'),
(676, 101, '_name12', 'field_5b237a1820b0f'),
(677, 101, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(678, 101, '_example12', 'field_5b237e4c4578e'),
(679, 101, 'grey1', 'Box 1'),
(680, 101, '_grey1', 'field_5b237e4c4578e'),
(681, 101, 'grey2', 'Box 2'),
(682, 101, '_grey2', 'field_5b2387af232ca'),
(683, 101, 'grey3', 'Box 3'),
(684, 101, '_grey3', 'field_5b2387bc232cb'),
(685, 101, 'grey4', 'Box 4'),
(686, 101, '_grey4', 'field_5b2387c4232cc'),
(687, 101, 'grey5', 'Box 5'),
(688, 101, '_grey5', 'field_5b2387ce232cd'),
(689, 101, 'photoleft', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(690, 101, '_photoleft', 'field_5b23883faea3e'),
(691, 101, 'photoright', 'Excepturi sint occaecati cupiditate accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(692, 101, '_photoright', 'field_5b23886faea3f'),
(693, 101, 'slide1', 'First Slide'),
(694, 101, '_slide1', 'field_5b28a8aed3e75'),
(695, 101, 'slide2', 'Second Slide'),
(696, 101, '_slide2', 'field_5b28a8c6d3e76'),
(697, 101, 'slide3', 'Third Slide'),
(698, 101, '_slide3', 'field_5b28a8dbd3e77'),
(699, 101, 'slide4', 'Fourth Slide');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(700, 101, '_slide4', 'field_5b28a8e8d3e78'),
(701, 101, 'slide5', 'Fifth Slide'),
(702, 101, '_slide5', 'field_5b28a8f3d3e79'),
(703, 114, '_wp_attached_file', '2018/06/1_29.jpg'),
(704, 114, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:387;s:6:"height";i:199;s:4:"file";s:16:"2018/06/1_29.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"1_29-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"1_29-300x154.jpg";s:5:"width";i:300;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(705, 115, '_wp_attached_file', '2018/06/1_31.jpg'),
(706, 115, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:387;s:6:"height";i:199;s:4:"file";s:16:"2018/06/1_31.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"1_31-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"1_31-300x154.jpg";s:5:"width";i:300;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(707, 6, 'projectimage1', '85'),
(708, 6, '_projectimage1', 'field_5b28ab57717ae'),
(709, 6, 'project1', 'Project 1'),
(710, 6, '_project1', 'field_5b28ab94717af'),
(711, 6, 'project1_link', '73'),
(712, 6, '_project1_link', 'field_5b28afb4717b0'),
(713, 6, 'project1-linkname', 'see more'),
(714, 6, '_project1-linkname', 'field_5b28aff4717b1'),
(715, 6, 'projectimage2', '114'),
(716, 6, '_projectimage2', 'field_5b28b044717b2'),
(717, 6, 'project2', 'Project 2'),
(718, 6, '_project2', 'field_5b28b06d717b3'),
(719, 6, 'project2_link', '73'),
(720, 6, '_project2_link', 'field_5b28b09a717b4'),
(721, 6, 'project2-linkname', 'see more'),
(722, 6, '_project2-linkname', 'field_5b28b0c4717b5'),
(723, 6, 'projectimage3', '115'),
(724, 6, '_projectimage3', 'field_5b28b1094651c'),
(725, 6, 'project3_link', '73'),
(726, 6, '_project3_link', 'field_5b28b1274651e'),
(727, 6, 'project3-linkname', 'see more'),
(728, 6, '_project3-linkname', 'field_5b28b0fb4651b'),
(729, 116, 'box1', 'sdcsdcccc'),
(730, 116, '_box1', 'field_5b21239d17b89'),
(731, 116, 'name12', 'Text to enter'),
(732, 116, '_name12', 'field_5b237a1820b0f'),
(733, 116, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(734, 116, '_example12', 'field_5b237e4c4578e'),
(735, 116, 'grey1', 'Box 1'),
(736, 116, '_grey1', 'field_5b237e4c4578e'),
(737, 116, 'grey2', 'Box 2'),
(738, 116, '_grey2', 'field_5b2387af232ca'),
(739, 116, 'grey3', 'Box 3'),
(740, 116, '_grey3', 'field_5b2387bc232cb'),
(741, 116, 'grey4', 'Box 4'),
(742, 116, '_grey4', 'field_5b2387c4232cc'),
(743, 116, 'grey5', 'Box 5'),
(744, 116, '_grey5', 'field_5b2387ce232cd'),
(745, 116, 'photoleft', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(746, 116, '_photoleft', 'field_5b23883faea3e'),
(747, 116, 'photoright', 'Excepturi sint occaecati cupiditate accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(748, 116, '_photoright', 'field_5b23886faea3f'),
(749, 116, 'slide1', 'First Slide'),
(750, 116, '_slide1', 'field_5b28a8aed3e75'),
(751, 116, 'slide2', 'Second Slide'),
(752, 116, '_slide2', 'field_5b28a8c6d3e76'),
(753, 116, 'slide3', 'Third Slide'),
(754, 116, '_slide3', 'field_5b28a8dbd3e77'),
(755, 116, 'slide4', 'Fourth Slide'),
(756, 116, '_slide4', 'field_5b28a8e8d3e78'),
(757, 116, 'slide5', 'Fifth Slide'),
(758, 116, '_slide5', 'field_5b28a8f3d3e79'),
(759, 116, 'projectimage1', '85'),
(760, 116, '_projectimage1', 'field_5b28ab57717ae'),
(761, 116, 'project1', 'Project 1'),
(762, 116, '_project1', 'field_5b28ab94717af'),
(763, 116, 'project1_link', '73'),
(764, 116, '_project1_link', 'field_5b28afb4717b0'),
(765, 116, 'project1-linkname', 'see more'),
(766, 116, '_project1-linkname', 'field_5b28aff4717b1'),
(767, 116, 'projectimage2', '114'),
(768, 116, '_projectimage2', 'field_5b28b044717b2'),
(769, 116, 'project2', 'Project 3'),
(770, 116, '_project2', 'field_5b28b1214651d'),
(771, 116, 'project2_link', '73'),
(772, 116, '_project2_link', 'field_5b28b09a717b4'),
(773, 116, 'project2-linkname', 'see more'),
(774, 116, '_project2-linkname', 'field_5b28b0c4717b5'),
(775, 116, 'projectimage3', '115'),
(776, 116, '_projectimage3', 'field_5b28b1094651c'),
(777, 116, 'project3_link', '73'),
(778, 116, '_project3_link', 'field_5b28b1274651e'),
(779, 116, 'project3-linkname', 'see more'),
(780, 116, '_project3-linkname', 'field_5b28b0fb4651b'),
(781, 6, 'project3', 'Project 3'),
(782, 6, '_project3', 'field_5b28b1214651d'),
(783, 117, 'box1', 'sdcsdcccc'),
(784, 117, '_box1', 'field_5b21239d17b89'),
(785, 117, 'name12', 'Text to enter'),
(786, 117, '_name12', 'field_5b237a1820b0f'),
(787, 117, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(788, 117, '_example12', 'field_5b237e4c4578e'),
(789, 117, 'grey1', 'Box 1'),
(790, 117, '_grey1', 'field_5b237e4c4578e'),
(791, 117, 'grey2', 'Box 2'),
(792, 117, '_grey2', 'field_5b2387af232ca'),
(793, 117, 'grey3', 'Box 3'),
(794, 117, '_grey3', 'field_5b2387bc232cb'),
(795, 117, 'grey4', 'Box 4'),
(796, 117, '_grey4', 'field_5b2387c4232cc'),
(797, 117, 'grey5', 'Box 5'),
(798, 117, '_grey5', 'field_5b2387ce232cd'),
(799, 117, 'photoleft', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(800, 117, '_photoleft', 'field_5b23883faea3e'),
(801, 117, 'photoright', 'Excepturi sint occaecati cupiditate accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(802, 117, '_photoright', 'field_5b23886faea3f'),
(803, 117, 'slide1', 'First Slide'),
(804, 117, '_slide1', 'field_5b28a8aed3e75'),
(805, 117, 'slide2', 'Second Slide'),
(806, 117, '_slide2', 'field_5b28a8c6d3e76'),
(807, 117, 'slide3', 'Third Slide'),
(808, 117, '_slide3', 'field_5b28a8dbd3e77'),
(809, 117, 'slide4', 'Fourth Slide'),
(810, 117, '_slide4', 'field_5b28a8e8d3e78'),
(811, 117, 'slide5', 'Fifth Slide'),
(812, 117, '_slide5', 'field_5b28a8f3d3e79'),
(813, 117, 'projectimage1', '85'),
(814, 117, '_projectimage1', 'field_5b28ab57717ae'),
(815, 117, 'project1', 'Project 1'),
(816, 117, '_project1', 'field_5b28ab94717af'),
(817, 117, 'project1_link', '73'),
(818, 117, '_project1_link', 'field_5b28afb4717b0'),
(819, 117, 'project1-linkname', 'see more'),
(820, 117, '_project1-linkname', 'field_5b28aff4717b1'),
(821, 117, 'projectimage2', '114'),
(822, 117, '_projectimage2', 'field_5b28b044717b2'),
(823, 117, 'project2', 'Project 2'),
(824, 117, '_project2', 'field_5b28b06d717b3'),
(825, 117, 'project2_link', '73'),
(826, 117, '_project2_link', 'field_5b28b09a717b4'),
(827, 117, 'project2-linkname', 'see more'),
(828, 117, '_project2-linkname', 'field_5b28b0c4717b5'),
(829, 117, 'projectimage3', '115'),
(830, 117, '_projectimage3', 'field_5b28b1094651c'),
(831, 117, 'project3_link', '73'),
(832, 117, '_project3_link', 'field_5b28b1274651e'),
(833, 117, 'project3-linkname', 'see more'),
(834, 117, '_project3-linkname', 'field_5b28b0fb4651b'),
(835, 117, 'project3', 'Project 3'),
(836, 117, '_project3', 'field_5b28b1214651d'),
(837, 6, 'projectimage123', '85'),
(838, 6, '_projectimage123', 'field_5b28b63711bbb'),
(839, 119, 'box1', 'sdcsdcccc'),
(840, 119, '_box1', 'field_5b21239d17b89'),
(841, 119, 'name12', 'Text to enter'),
(842, 119, '_name12', 'field_5b237a1820b0f'),
(843, 119, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(844, 119, '_example12', 'field_5b237e4c4578e'),
(845, 119, 'grey1', 'Box 1'),
(846, 119, '_grey1', 'field_5b237e4c4578e'),
(847, 119, 'grey2', 'Box 2'),
(848, 119, '_grey2', 'field_5b2387af232ca'),
(849, 119, 'grey3', 'Box 3'),
(850, 119, '_grey3', 'field_5b2387bc232cb'),
(851, 119, 'grey4', 'Box 4'),
(852, 119, '_grey4', 'field_5b2387c4232cc'),
(853, 119, 'grey5', 'Box 5'),
(854, 119, '_grey5', 'field_5b2387ce232cd'),
(855, 119, 'photoleft', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(856, 119, '_photoleft', 'field_5b23883faea3e'),
(857, 119, 'photoright', 'Excepturi sint occaecati cupiditate accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(858, 119, '_photoright', 'field_5b23886faea3f'),
(859, 119, 'slide1', 'First Slide'),
(860, 119, '_slide1', 'field_5b28a8aed3e75'),
(861, 119, 'slide2', 'Second Slide'),
(862, 119, '_slide2', 'field_5b28a8c6d3e76'),
(863, 119, 'slide3', 'Third Slide'),
(864, 119, '_slide3', 'field_5b28a8dbd3e77'),
(865, 119, 'slide4', 'Fourth Slide'),
(866, 119, '_slide4', 'field_5b28a8e8d3e78'),
(867, 119, 'slide5', 'Fifth Slide'),
(868, 119, '_slide5', 'field_5b28a8f3d3e79'),
(869, 119, 'projectimage1', '85'),
(870, 119, '_projectimage1', 'field_5b28ab57717ae'),
(871, 119, 'project1', 'Project 1'),
(872, 119, '_project1', 'field_5b28ab94717af'),
(873, 119, 'project1_link', '73'),
(874, 119, '_project1_link', 'field_5b28afb4717b0'),
(875, 119, 'project1-linkname', 'see more'),
(876, 119, '_project1-linkname', 'field_5b28aff4717b1'),
(877, 119, 'projectimage2', '114'),
(878, 119, '_projectimage2', 'field_5b28b044717b2'),
(879, 119, 'project2', 'Project 2'),
(880, 119, '_project2', 'field_5b28b06d717b3'),
(881, 119, 'project2_link', '73'),
(882, 119, '_project2_link', 'field_5b28b09a717b4'),
(883, 119, 'project2-linkname', 'see more'),
(884, 119, '_project2-linkname', 'field_5b28b0c4717b5'),
(885, 119, 'projectimage3', '115'),
(886, 119, '_projectimage3', 'field_5b28b1094651c'),
(887, 119, 'project3_link', '73'),
(888, 119, '_project3_link', 'field_5b28b1274651e'),
(889, 119, 'project3-linkname', 'see more'),
(890, 119, '_project3-linkname', 'field_5b28b0fb4651b'),
(891, 119, 'project3', 'Project 3'),
(892, 119, '_project3', 'field_5b28b1214651d'),
(893, 119, 'projectimage123', '85'),
(894, 119, '_projectimage123', 'field_5b28b63711bbb'),
(895, 6, 'image_test', '85'),
(896, 6, '_image_test', 'field_5b28b7ba73479'),
(897, 121, 'box1', 'sdcsdcccc'),
(898, 121, '_box1', 'field_5b21239d17b89'),
(899, 121, 'name12', 'Text to enter'),
(900, 121, '_name12', 'field_5b237a1820b0f'),
(901, 121, 'example12', 'nskjcnslkjdncalksjdcn\r\nsdcsdc\r\ns\r\nsdc'),
(902, 121, '_example12', 'field_5b237e4c4578e'),
(903, 121, 'grey1', 'Box 1'),
(904, 121, '_grey1', 'field_5b237e4c4578e'),
(905, 121, 'grey2', 'Box 2'),
(906, 121, '_grey2', 'field_5b2387af232ca'),
(907, 121, 'grey3', 'Box 3'),
(908, 121, '_grey3', 'field_5b2387bc232cb'),
(909, 121, 'grey4', 'Box 4'),
(910, 121, '_grey4', 'field_5b2387c4232cc'),
(911, 121, 'grey5', 'Box 5'),
(912, 121, '_grey5', 'field_5b2387ce232cd'),
(913, 121, 'photoleft', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(914, 121, '_photoleft', 'field_5b23883faea3e'),
(915, 121, 'photoright', 'Excepturi sint occaecati cupiditate accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga dolores et quas molestias excepturi sint occaecati cupiditate.'),
(916, 121, '_photoright', 'field_5b23886faea3f'),
(917, 121, 'slide1', 'First Slide'),
(918, 121, '_slide1', 'field_5b28a8aed3e75'),
(919, 121, 'slide2', 'Second Slide'),
(920, 121, '_slide2', 'field_5b28a8c6d3e76'),
(921, 121, 'slide3', 'Third Slide'),
(922, 121, '_slide3', 'field_5b28a8dbd3e77'),
(923, 121, 'slide4', 'Fourth Slide'),
(924, 121, '_slide4', 'field_5b28a8e8d3e78'),
(925, 121, 'slide5', 'Fifth Slide'),
(926, 121, '_slide5', 'field_5b28a8f3d3e79'),
(927, 121, 'projectimage1', '85'),
(928, 121, '_projectimage1', 'field_5b28ab57717ae'),
(929, 121, 'project1', 'Project 1'),
(930, 121, '_project1', 'field_5b28ab94717af'),
(931, 121, 'project1_link', '73'),
(932, 121, '_project1_link', 'field_5b28afb4717b0'),
(933, 121, 'project1-linkname', 'see more'),
(934, 121, '_project1-linkname', 'field_5b28aff4717b1'),
(935, 121, 'projectimage2', '114'),
(936, 121, '_projectimage2', 'field_5b28b044717b2'),
(937, 121, 'project2', 'Project 2'),
(938, 121, '_project2', 'field_5b28b06d717b3'),
(939, 121, 'project2_link', '73'),
(940, 121, '_project2_link', 'field_5b28b09a717b4'),
(941, 121, 'project2-linkname', 'see more'),
(942, 121, '_project2-linkname', 'field_5b28b0c4717b5'),
(943, 121, 'projectimage3', '115'),
(944, 121, '_projectimage3', 'field_5b28b1094651c'),
(945, 121, 'project3_link', '73'),
(946, 121, '_project3_link', 'field_5b28b1274651e'),
(947, 121, 'project3-linkname', 'see more'),
(948, 121, '_project3-linkname', 'field_5b28b0fb4651b'),
(949, 121, 'project3', 'Project 3'),
(950, 121, '_project3', 'field_5b28b1214651d'),
(951, 121, 'projectimage123', '85'),
(952, 121, '_projectimage123', 'field_5b28b63711bbb'),
(953, 121, 'image_test', '85'),
(954, 121, '_image_test', 'field_5b28b7ba73479'),
(955, 122, 'abouttextleft', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.'),
(956, 122, '_abouttextleft', 'field_5b238e9d9c51a'),
(957, 122, 'abouttextright', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'),
(958, 122, '_abouttextright', 'field_5b238ece9c51b'),
(959, 122, 'quote', 'Two things are infinite: the universe and human stupidity; and I''m not sure about the universe'),
(960, 122, '_quote', 'field_5b239037b29b4'),
(961, 122, 'quoteauthor', 'Albert Einstein'),
(962, 122, '_quoteauthor', 'field_5b23906ed79be'),
(963, 122, 'textonimage-name', 'John Smith'),
(964, 122, '_textonimage-name', 'field_5b23915430599'),
(965, 122, 'textonimage', 'WordPress is software designed for everyone, emphasizing accessibility, performance, security, and ease of use. We believe great software should work with minimum set up, so you can focus on sharing your story, product, or services freely.'),
(966, 122, '_textonimage', 'field_5b23918d3059a'),
(967, 124, '_edit_last', '1'),
(968, 124, '_edit_lock', '1529399259:1'),
(969, 124, '_wp_page_template', 'footer.php'),
(970, 124, '_wp_trash_meta_status', 'publish'),
(971, 124, '_wp_trash_meta_time', '1529399407'),
(972, 124, '_wp_desired_post_slug', 'footer'),
(973, 126, '_edit_last', '1'),
(974, 126, '_edit_lock', '1529406908:1'),
(975, 128, '_edit_last', '1'),
(976, 128, '_wp_page_template', 'header.php'),
(977, 128, '_edit_lock', '1529406749:1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-06-13 15:43:00', '2018-06-13 13:43:00', 'Witaj w WordPressie. To jest twój pierwszy wpis. Zmodyfikuj go lub usuń, a następnie rozpocznij pisanie!', 'Witaj, świecie!', '', 'publish', 'open', 'open', '', 'witaj-swiecie', '', '', '2018-06-13 15:43:00', '2018-06-13 13:43:00', '', 0, 'http://localhost/idea2/?p=1', 0, 'post', '', 1),
(2, 1, '2018-06-13 15:43:00', '2018-06-13 13:43:00', 'To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\n\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię pi&#241;a coladę (a także spacery, gdy pada deszcz).</blockquote>\n\n... lub taki:\n\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\n\nJako nowy użytkownik WordPressa powinieneś przejść do <a href="http://localhost/idea2/wp-admin/">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!', 'Przykładowa strona', '', 'trash', 'closed', 'open', '', 'przykladowa-strona__trashed', '', '', '2018-06-18 08:25:42', '2018-06-18 06:25:42', '', 0, 'http://localhost/idea2/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-06-13 15:43:00', '2018-06-13 13:43:00', '<h2>Kim jesteśmy</h2><p>Adres naszej strony internetowej to: http://localhost/idea2.</p><h2>Jakie dane osobiste zbieramy i dlaczego je zbieramy</h2><h3>Komentarze</h3><p>Kiedy odwiedzający witrynę zostawia komentarz, zbieramy dane widoczne w formularzu komentowania, jak i adres IP odwiedzającego oraz podpis jego przeglądarki jako pomoc przy wykrywaniu spamu.</p><p>Zanonimizowany ciąg znaków stworzony na podstawie twojego adresu email (tak zwany hash) może zostać przesłany do usługi Gravatar w celu sprawdzenia czy jej używasz. Polityka prywatności usługi Gravatar jest dostępna tutaj: https://automattic.com/privacy/. Po zatwierdzeniu komentarza twój obrazek profilowy jest widoczny publicznie w kontekście twojego komentarza.</p><h3>Media</h3><p>Jeśli jesteś zarejestrowanym użytkownikiem i wgrywasz na witrynę obrazki, powinieneś unikać przesyłania obrazków z tagami EXIF lokalizacji. Odwiedzający stronę mogą pobrać i odczytać pełne dane lokalizacyjne z obrazków w witrynie.</p><h3>Formularze kontaktowe</h3><h3>Ciasteczka</h3><p>Jeśli zostawisz na naszej witrynie komentarz, będziesz mógł wybrać opcję zapisu twojej nazwy, adresu email i adresu strony internetowej w ciasteczkach, dzięki którym podczas pisania kolejnych komentarzy powyższe informacje będą już dogodnie uzupełnione. Te ciasteczka wygasają po roku.</p><p>Jeśli masz konto i zalogujesz się na tej witrynie, utworzymy tymczasowe ciasteczko na potrzeby sprawdzenia czy twoja przeglądarka akceptuje ciasteczka. To ciasteczko nie zawiera żadnych danych osobistych i zostanie wyrzucone kiedy zamkniesz przeglądarkę.</p><p>Podczas logowania tworzymy dodatkowo kilka ciasteczek potrzebnych do zapisu twoich informacji logowania oraz wybranych opcji ekranu. Ciasteczka logowania wygasają po dwóch dniach, a opcji ekranu po roku. Jeśli zaznaczysz opcję &bdquo;Pamiętaj mnie&rdquo;, logowanie wygaśnie po dwóch tygodniach. Jeśli wylogujesz się ze swojego konta, ciasteczka logowania zostaną usunięte.</p><p>Jeśli zmodyfikujesz albo opublikujesz artykuł, w twojej przeglądarce zostanie zapisane dodatkowe ciasteczko. To ciasteczko nie zawiera żadnych danych osobistych, wskazując po prostu na identyfikator przed chwilą edytowanego artykułu. Wygasa ono po 1 dniu.</p><h3>Osadzone treści z innych witryn</h3><p>Artykuły na tej witrynie mogą zawierać osadzone treści (np. filmy, obrazki, artykuły itp.). Osadzone treści z innych witryn zachowują się analogicznie do tego, jakby użytkownik odwiedził bezpośrednio konkretną witrynę.</p><p>Witryny mogą zbierać informacje o tobie, używać ciasteczek, dołączać dodatkowe, zewnętrzne systemy śledzenia i monitorować twoje interakcje z osadzonym materiałem, włączając w to śledzenie twoich interakcji z osadzonym materiałem jeśli posiadasz konto i jesteś zalogowany w tamtej witrynie.</p><h3>Analiza statystyk</h3><h2>Z kim dzielimy się danymi</h2><h2>Jak długo przechowujemy twoje dane</h2><p>Jeśli zostawisz komentarz, jego treść i metadane będą przechowywane przez czas nieokreślony. Dzięki temu jesteśmy w stanie rozpoznawać i zatwierdzać kolejne komentarze automatycznie, bez wysyłania ich do każdorazowej moderacji.</p><p>Dla użytkowników którzy zarejestrowali się na naszej stronie internetowej (jeśli tacy są), przechowujemy również informacje osobiste wprowadzone w profilu. Każdy użytkownik może dokonać wglądu, korekty albo skasować swoje informacje osobiste w dowolnej chwili (nie licząc nazwy użytkownika, której nie można zmienić). Administratorzy strony również mogą przeglądać i modyfikować te informacje.</p><h2>Jakie masz prawa do swoich danych</h2><p>Jeśli masz konto użytkownika albo dodałeś komentarze w tej witrynie, możesz zażądać dostarczenia pliku z wyeksportowanym kompletem twoich danych osobistych będących w naszym posiadaniu, w tym całość tych dostarczonych przez ciebie. Możesz również zażądać usunięcia przez nas całości twoich danych osobistych w naszym posiadaniu. Nie dotyczy to żadnych danych które jesteśmy zobligowani zachować ze względów administracyjnych, prawnych albo bezpieczeństwa.</p><h2>Gdzie przesyłamy dane</h2><p>Komentarze gości mogą być sprawdzane za pomocą automatycznej usługi wykrywania spamu.</p><h2>Twoje dane kontaktowe</h2><h2>Informacje dodatkowe</h2><h3>Jak chronimy twoje dane?</h3><h3>Jakie mamy obowiązujące procedury w przypadku naruszenia prywatności danych</h3><h3>Od jakich stron trzecich otrzymujemy dane</h3><h3>Jakie automatyczne podejmowanie decyzji i/lub tworzenie profili przeprowadzamy z użyciem danych użytkownika</h3><h3>Branżowe wymogi regulacyjne dotyczące ujawniania informacji</h3>', 'Polityka prywatności', '', 'trash', 'closed', 'open', '', 'polityka-prywatnosci__trashed', '', '', '2018-06-18 08:25:25', '2018-06-18 06:25:25', '', 0, 'http://localhost/idea2/?page_id=3', 0, 'page', '', 0),
(4, 1, '2018-06-13 15:43:38', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-06-13 15:43:38', '0000-00-00 00:00:00', '', 0, 'http://localhost/idea2/?p=4', 0, 'post', '', 0),
(5, 1, '2018-06-15 14:16:36', '2018-06-15 12:16:36', '', '', '', 'trash', 'closed', 'closed', '', '__trashed', '', '', '2018-06-15 14:16:36', '2018-06-15 12:16:36', '', 0, 'http://localhost/idea2/?page_id=5', 0, 'page', '', 0),
(6, 1, '2018-06-13 15:57:16', '2018-06-13 13:57:16', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-06-19 10:00:41', '2018-06-19 08:00:41', '', 0, 'http://localhost/idea2/?page_id=6', 0, 'page', '', 0),
(7, 1, '2018-06-13 15:57:16', '2018-06-13 13:57:16', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-13 15:57:16', '2018-06-13 13:57:16', '', 6, 'http://localhost/idea2/2018/06/13/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2018-06-13 16:03:06', '2018-06-13 14:03:06', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:13:"frontpage.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:14:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:14:"featured_image";i:11;s:10:"categories";i:12;s:4:"tags";i:13;s:15:"send-trackbacks";}s:11:"description";s:0:"";}', 'Home', 'home', 'publish', 'closed', 'closed', '', 'group_5b212398b50d7', '', '', '2018-06-19 11:24:58', '2018-06-19 09:24:58', '', 0, 'http://localhost/idea2/?post_type=acf-field-group&#038;p=8', 0, 'acf-field-group', '', 0),
(10, 1, '2018-06-14 11:55:21', '0000-00-00 00:00:00', '', 'Strona główna', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-14 11:55:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/idea2/?p=10', 1, 'nav_menu_item', '', 0),
(11, 1, '2018-06-14 11:55:22', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-14 11:55:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/idea2/?p=11', 1, 'nav_menu_item', '', 0),
(12, 1, '2018-06-14 11:55:22', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-14 11:55:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/idea2/?p=12', 1, 'nav_menu_item', '', 0),
(13, 1, '2018-06-14 12:28:40', '2018-06-14 10:28:40', '', 'Home', '', 'publish', 'closed', 'closed', '', 'strona-glowna', '', '', '2018-06-15 12:43:38', '2018-06-15 10:43:38', '', 0, 'http://localhost/idea2/?p=13', 1, 'nav_menu_item', '', 0),
(16, 1, '2018-06-14 12:36:24', '2018-06-14 10:36:24', '', 'Projects', '', 'publish', 'closed', 'closed', '', 'google', '', '', '2018-06-15 12:43:38', '2018-06-15 10:43:38', '', 0, 'http://localhost/idea2/?p=16', 4, 'nav_menu_item', '', 0),
(17, 1, '2018-06-14 13:17:28', '2018-06-14 11:17:28', '', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2018-06-15 12:43:39', '2018-06-15 10:43:39', '', 0, 'http://localhost/idea2/?p=17', 5, 'nav_menu_item', '', 0),
(18, 1, '2018-06-14 14:38:04', '2018-06-14 12:38:04', '', 'About the company', '', 'publish', 'closed', 'closed', '', 'about-the-company', '', '', '2018-06-15 12:43:38', '2018-06-15 10:43:38', '', 0, 'http://localhost/idea2/?p=18', 2, 'nav_menu_item', '', 0),
(19, 1, '2018-06-14 14:38:04', '2018-06-14 12:38:04', '', 'Job offers', '', 'publish', 'closed', 'closed', '', 'job-offers', '', '', '2018-06-15 12:43:38', '2018-06-15 10:43:38', '', 0, 'http://localhost/idea2/?p=19', 3, 'nav_menu_item', '', 0),
(20, 1, '2018-06-15 10:10:17', '2018-06-15 08:10:17', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-15 10:10:17', '2018-06-15 08:10:17', '', 6, 'http://localhost/idea2/2018/06/15/6-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2018-06-15 10:44:44', '2018-06-15 08:44:44', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-15 10:44:44', '2018-06-15 08:44:44', '', 6, 'http://localhost/idea2/2018/06/15/6-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2018-06-15 10:49:06', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-06-15 10:49:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/idea2/?page_id=25', 0, 'page', '', 0),
(26, 1, '2018-06-15 10:52:55', '2018-06-15 08:52:55', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'First text from the left under the slider', 'grey1', 'publish', 'closed', 'closed', '', 'field_5b237e4c4578e', '', '', '2018-06-19 08:58:06', '2018-06-19 06:58:06', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=26', 5, 'acf-field', '', 0),
(27, 1, '2018-06-15 10:58:00', '2018-06-15 08:58:00', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-15 10:58:00', '2018-06-15 08:58:00', '', 6, 'http://localhost/idea2/2018/06/15/6-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2018-06-15 11:09:44', '2018-06-15 09:09:44', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-autosave-v1', '', '', '2018-06-15 11:09:44', '2018-06-15 09:09:44', '', 6, 'http://localhost/idea2/2018/06/15/6-autosave-v1/', 0, 'revision', '', 0),
(30, 1, '2018-06-15 11:20:13', '2018-06-15 09:20:13', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-15 11:20:13', '2018-06-15 09:20:13', '', 6, 'http://localhost/idea2/2018/06/15/6-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2018-06-15 11:20:33', '2018-06-15 09:20:33', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-15 11:20:33', '2018-06-15 09:20:33', '', 6, 'http://localhost/idea2/2018/06/15/6-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2018-06-15 11:34:44', '2018-06-15 09:34:44', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Second text from the left under the slider', 'grey2', 'publish', 'closed', 'closed', '', 'field_5b2387af232ca', '', '', '2018-06-19 08:58:06', '2018-06-19 06:58:06', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=32', 6, 'acf-field', '', 0),
(33, 1, '2018-06-15 11:34:44', '2018-06-15 09:34:44', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Third text from the left under the slider', 'grey3', 'publish', 'closed', 'closed', '', 'field_5b2387bc232cb', '', '', '2018-06-19 08:58:06', '2018-06-19 06:58:06', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=33', 7, 'acf-field', '', 0),
(34, 1, '2018-06-15 11:34:44', '2018-06-15 09:34:44', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Fourth text from the left under the slider', 'grey4', 'publish', 'closed', 'closed', '', 'field_5b2387c4232cc', '', '', '2018-06-19 08:58:06', '2018-06-19 06:58:06', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=34', 8, 'acf-field', '', 0),
(35, 1, '2018-06-15 11:34:44', '2018-06-15 09:34:44', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Fifth text from the left under the slider', 'grey5', 'publish', 'closed', 'closed', '', 'field_5b2387ce232cd', '', '', '2018-06-19 08:58:06', '2018-06-19 06:58:06', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=35', 9, 'acf-field', '', 0),
(36, 1, '2018-06-15 11:36:03', '2018-06-15 09:36:03', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Left text', 'photoleft', 'publish', 'closed', 'closed', '', 'field_5b23883faea3e', '', '', '2018-06-19 08:58:06', '2018-06-19 06:58:06', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=36', 10, 'acf-field', '', 0),
(37, 1, '2018-06-15 11:36:03', '2018-06-15 09:36:03', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Right text', 'photoright', 'publish', 'closed', 'closed', '', 'field_5b23886faea3f', '', '', '2018-06-19 08:58:06', '2018-06-19 06:58:06', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=37', 11, 'acf-field', '', 0),
(38, 1, '2018-06-15 11:37:30', '2018-06-15 09:37:30', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-15 11:37:30', '2018-06-15 09:37:30', '', 6, 'http://localhost/idea2/2018/06/15/6-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2018-06-15 11:40:22', '2018-06-15 09:40:22', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-15 11:40:22', '2018-06-15 09:40:22', '', 6, 'http://localhost/idea2/2018/06/15/6-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2018-06-15 11:40:59', '2018-06-15 09:40:59', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-15 11:40:59', '2018-06-15 09:40:59', '', 6, 'http://localhost/idea2/2018/06/15/6-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2018-06-15 11:53:25', '2018-06-15 09:53:25', '', 'About', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2018-06-19 10:24:54', '2018-06-19 08:24:54', '', 0, 'http://localhost/idea2/?page_id=41', 0, 'page', '', 0),
(42, 1, '2018-06-15 11:53:25', '2018-06-15 09:53:25', '', 'About', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-06-15 11:53:25', '2018-06-15 09:53:25', '', 41, 'http://localhost/idea2/2018/06/15/41-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2018-06-15 12:01:59', '2018-06-15 10:01:59', 'a:7:{s:8:"location";a:1:{i:0;a:2:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:9:"about.php";}i:1;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:9:"about.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:14:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:14:"featured_image";i:11;s:10:"categories";i:12;s:4:"tags";i:13;s:15:"send-trackbacks";}s:11:"description";s:0:"";}', 'About', 'about', 'publish', 'closed', 'closed', '', 'group_5b238e80933b2', '', '', '2018-06-15 14:17:21', '2018-06-15 12:17:21', '', 0, 'http://localhost/idea2/?post_type=acf-field-group&#038;p=43', 0, 'acf-field-group', '', 0),
(44, 1, '2018-06-15 12:03:23', '2018-06-15 10:03:23', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'First text next to the image', 'abouttextleft', 'publish', 'closed', 'closed', '', 'field_5b238e9d9c51a', '', '', '2018-06-15 12:03:23', '2018-06-15 10:03:23', '', 43, 'http://localhost/idea2/?post_type=acf-field&p=44', 0, 'acf-field', '', 0),
(45, 1, '2018-06-15 12:03:23', '2018-06-15 10:03:23', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Second text next to the image', 'abouttextright', 'publish', 'closed', 'closed', '', 'field_5b238ece9c51b', '', '', '2018-06-15 12:03:37', '2018-06-15 10:03:37', '', 43, 'http://localhost/idea2/?post_type=acf-field&#038;p=45', 1, 'acf-field', '', 0),
(46, 1, '2018-06-15 12:07:02', '2018-06-15 10:07:02', '', 'About', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-06-15 12:07:02', '2018-06-15 10:07:02', '', 41, 'http://localhost/idea2/2018/06/15/41-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2018-06-15 12:09:24', '2018-06-15 10:09:24', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Quote', 'quote', 'publish', 'closed', 'closed', '', 'field_5b239037b29b4', '', '', '2018-06-15 12:09:24', '2018-06-15 10:09:24', '', 43, 'http://localhost/idea2/?post_type=acf-field&p=47', 2, 'acf-field', '', 0),
(48, 1, '2018-06-15 12:10:02', '2018-06-15 10:10:02', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Quote Author', 'quoteauthor', 'publish', 'closed', 'closed', '', 'field_5b23906ed79be', '', '', '2018-06-15 12:10:02', '2018-06-15 10:10:02', '', 43, 'http://localhost/idea2/?post_type=acf-field&p=48', 3, 'acf-field', '', 0),
(49, 1, '2018-06-15 12:11:17', '2018-06-15 10:11:17', '', 'About', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-06-15 12:11:17', '2018-06-15 10:11:17', '', 41, 'http://localhost/idea2/2018/06/15/41-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2018-06-15 12:14:58', '2018-06-15 10:14:58', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Name above the text below quote', 'textonimage-name', 'publish', 'closed', 'closed', '', 'field_5b23915430599', '', '', '2018-06-15 12:14:58', '2018-06-15 10:14:58', '', 43, 'http://localhost/idea2/?post_type=acf-field&p=50', 4, 'acf-field', '', 0),
(51, 1, '2018-06-15 12:14:58', '2018-06-15 10:14:58', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Text below quote', 'textonimage', 'publish', 'closed', 'closed', '', 'field_5b23918d3059a', '', '', '2018-06-15 12:14:58', '2018-06-15 10:14:58', '', 43, 'http://localhost/idea2/?post_type=acf-field&p=51', 5, 'acf-field', '', 0),
(52, 1, '2018-06-15 12:16:07', '2018-06-15 10:16:07', '', 'About', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-06-15 12:16:07', '2018-06-15 10:16:07', '', 41, 'http://localhost/idea2/2018/06/15/41-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2018-06-15 12:16:47', '2018-06-15 10:16:47', '', 'About', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-06-15 12:16:47', '2018-06-15 10:16:47', '', 41, 'http://localhost/idea2/2018/06/15/41-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2018-06-15 12:42:30', '2018-06-15 10:42:30', '', 'Job Offers', '', 'publish', 'closed', 'closed', '', 'job-offers', '', '', '2018-06-15 14:15:37', '2018-06-15 12:15:37', '', 0, 'http://localhost/idea2/?page_id=54', 0, 'page', '', 0),
(55, 1, '2018-06-15 12:42:30', '2018-06-15 10:42:30', '', 'Job Offers', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-06-15 12:42:30', '2018-06-15 10:42:30', '', 54, 'http://localhost/idea2/2018/06/15/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2018-06-15 12:45:21', '2018-06-15 10:45:21', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:13:"joboffers.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:14:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:14:"featured_image";i:11;s:10:"categories";i:12;s:4:"tags";i:13;s:15:"send-trackbacks";}s:11:"description";s:0:"";}', 'Job Offers', 'job-offers', 'publish', 'closed', 'closed', '', 'group_5b2398b0f1453', '', '', '2018-06-15 14:18:20', '2018-06-15 12:18:20', '', 0, 'http://localhost/idea2/?post_type=acf-field-group&#038;p=56', 0, 'acf-field-group', '', 0),
(59, 1, '2018-06-15 12:55:55', '2018-06-15 10:55:55', '', 'Job Offers', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-06-15 12:55:55', '2018-06-15 10:55:55', '', 54, 'http://localhost/idea2/2018/06/15/54-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2018-06-15 12:58:34', '2018-06-15 10:58:34', '', 'Job Offers', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-06-15 12:58:34', '2018-06-15 10:58:34', '', 54, 'http://localhost/idea2/2018/06/15/54-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2018-06-15 13:03:17', '2018-06-15 11:03:17', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";}', 'Job Offer', 'job_offer', 'publish', 'closed', 'closed', '', 'field_5b239cbd68544', '', '', '2018-06-15 13:03:17', '2018-06-15 11:03:17', '', 56, 'http://localhost/idea2/?post_type=acf-field&p=62', 0, 'acf-field', '', 0),
(63, 1, '2018-06-15 13:03:17', '2018-06-15 11:03:17', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Job Name', 'job_name', 'publish', 'closed', 'closed', '', 'field_5b239cd768545', '', '', '2018-06-15 13:03:17', '2018-06-15 11:03:17', '', 62, 'http://localhost/idea2/?post_type=acf-field&p=63', 0, 'acf-field', '', 0),
(64, 1, '2018-06-15 13:03:17', '2018-06-15 11:03:17', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Job Description', 'job_description', 'publish', 'closed', 'closed', '', 'field_5b239ce968546', '', '', '2018-06-15 13:03:17', '2018-06-15 11:03:17', '', 62, 'http://localhost/idea2/?post_type=acf-field&p=64', 1, 'acf-field', '', 0),
(65, 1, '2018-06-15 13:06:14', '2018-06-15 11:06:14', '', 'Job Offers', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-06-15 13:06:14', '2018-06-15 11:06:14', '', 54, 'http://localhost/idea2/2018/06/15/54-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2018-06-15 13:11:21', '2018-06-15 11:11:21', 'a:10:{s:4:"type";s:9:"page_link";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:0:{}s:8:"taxonomy";a:0:{}s:10:"allow_null";i:0;s:14:"allow_archives";i:1;s:8:"multiple";i:0;}', 'Job link', 'job_link', 'publish', 'closed', 'closed', '', 'field_5b239ec2ea85f', '', '', '2018-06-15 13:11:21', '2018-06-15 11:11:21', '', 62, 'http://localhost/idea2/?post_type=acf-field&p=66', 2, 'acf-field', '', 0),
(67, 1, '2018-06-15 13:12:34', '2018-06-15 11:12:34', '', 'Job Offers', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-06-15 13:12:34', '2018-06-15 11:12:34', '', 54, 'http://localhost/idea2/2018/06/15/54-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2018-06-15 13:14:12', '2018-06-15 11:14:12', '', 'Job Offers', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-06-15 13:14:12', '2018-06-15 11:14:12', '', 54, 'http://localhost/idea2/2018/06/15/54-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2018-06-15 13:44:02', '2018-06-15 11:44:02', '', 'Job Offers', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-06-15 13:44:02', '2018-06-15 11:44:02', '', 54, 'http://localhost/idea2/2018/06/15/54-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2018-06-15 14:15:37', '2018-06-15 12:15:37', '', 'Job Offers', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-06-15 14:15:37', '2018-06-15 12:15:37', '', 54, 'http://localhost/idea2/2018/06/15/54-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2018-06-15 14:16:26', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-06-15 14:16:26', '0000-00-00 00:00:00', '', 0, 'http://localhost/idea2/?page_id=71', 0, 'page', '', 0),
(72, 1, '2018-06-15 14:16:36', '2018-06-15 12:16:36', '', '', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-06-15 14:16:36', '2018-06-15 12:16:36', '', 5, 'http://localhost/idea2/2018/06/15/5-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2018-06-15 14:30:06', '2018-06-15 12:30:06', '', 'Projects', '', 'publish', 'closed', 'closed', '', 'projects', '', '', '2018-06-18 10:27:25', '2018-06-18 08:27:25', '', 0, 'http://localhost/idea2/?page_id=73', 0, 'page', '', 0),
(74, 1, '2018-06-15 14:30:06', '2018-06-15 12:30:06', '', 'Projects', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-06-15 14:30:06', '2018-06-15 12:30:06', '', 73, 'http://localhost/idea2/2018/06/15/73-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2018-06-18 08:25:25', '2018-06-18 06:25:25', '<h2>Kim jesteśmy</h2><p>Adres naszej strony internetowej to: http://localhost/idea2.</p><h2>Jakie dane osobiste zbieramy i dlaczego je zbieramy</h2><h3>Komentarze</h3><p>Kiedy odwiedzający witrynę zostawia komentarz, zbieramy dane widoczne w formularzu komentowania, jak i adres IP odwiedzającego oraz podpis jego przeglądarki jako pomoc przy wykrywaniu spamu.</p><p>Zanonimizowany ciąg znaków stworzony na podstawie twojego adresu email (tak zwany hash) może zostać przesłany do usługi Gravatar w celu sprawdzenia czy jej używasz. Polityka prywatności usługi Gravatar jest dostępna tutaj: https://automattic.com/privacy/. Po zatwierdzeniu komentarza twój obrazek profilowy jest widoczny publicznie w kontekście twojego komentarza.</p><h3>Media</h3><p>Jeśli jesteś zarejestrowanym użytkownikiem i wgrywasz na witrynę obrazki, powinieneś unikać przesyłania obrazków z tagami EXIF lokalizacji. Odwiedzający stronę mogą pobrać i odczytać pełne dane lokalizacyjne z obrazków w witrynie.</p><h3>Formularze kontaktowe</h3><h3>Ciasteczka</h3><p>Jeśli zostawisz na naszej witrynie komentarz, będziesz mógł wybrać opcję zapisu twojej nazwy, adresu email i adresu strony internetowej w ciasteczkach, dzięki którym podczas pisania kolejnych komentarzy powyższe informacje będą już dogodnie uzupełnione. Te ciasteczka wygasają po roku.</p><p>Jeśli masz konto i zalogujesz się na tej witrynie, utworzymy tymczasowe ciasteczko na potrzeby sprawdzenia czy twoja przeglądarka akceptuje ciasteczka. To ciasteczko nie zawiera żadnych danych osobistych i zostanie wyrzucone kiedy zamkniesz przeglądarkę.</p><p>Podczas logowania tworzymy dodatkowo kilka ciasteczek potrzebnych do zapisu twoich informacji logowania oraz wybranych opcji ekranu. Ciasteczka logowania wygasają po dwóch dniach, a opcji ekranu po roku. Jeśli zaznaczysz opcję &bdquo;Pamiętaj mnie&rdquo;, logowanie wygaśnie po dwóch tygodniach. Jeśli wylogujesz się ze swojego konta, ciasteczka logowania zostaną usunięte.</p><p>Jeśli zmodyfikujesz albo opublikujesz artykuł, w twojej przeglądarce zostanie zapisane dodatkowe ciasteczko. To ciasteczko nie zawiera żadnych danych osobistych, wskazując po prostu na identyfikator przed chwilą edytowanego artykułu. Wygasa ono po 1 dniu.</p><h3>Osadzone treści z innych witryn</h3><p>Artykuły na tej witrynie mogą zawierać osadzone treści (np. filmy, obrazki, artykuły itp.). Osadzone treści z innych witryn zachowują się analogicznie do tego, jakby użytkownik odwiedził bezpośrednio konkretną witrynę.</p><p>Witryny mogą zbierać informacje o tobie, używać ciasteczek, dołączać dodatkowe, zewnętrzne systemy śledzenia i monitorować twoje interakcje z osadzonym materiałem, włączając w to śledzenie twoich interakcji z osadzonym materiałem jeśli posiadasz konto i jesteś zalogowany w tamtej witrynie.</p><h3>Analiza statystyk</h3><h2>Z kim dzielimy się danymi</h2><h2>Jak długo przechowujemy twoje dane</h2><p>Jeśli zostawisz komentarz, jego treść i metadane będą przechowywane przez czas nieokreślony. Dzięki temu jesteśmy w stanie rozpoznawać i zatwierdzać kolejne komentarze automatycznie, bez wysyłania ich do każdorazowej moderacji.</p><p>Dla użytkowników którzy zarejestrowali się na naszej stronie internetowej (jeśli tacy są), przechowujemy również informacje osobiste wprowadzone w profilu. Każdy użytkownik może dokonać wglądu, korekty albo skasować swoje informacje osobiste w dowolnej chwili (nie licząc nazwy użytkownika, której nie można zmienić). Administratorzy strony również mogą przeglądać i modyfikować te informacje.</p><h2>Jakie masz prawa do swoich danych</h2><p>Jeśli masz konto użytkownika albo dodałeś komentarze w tej witrynie, możesz zażądać dostarczenia pliku z wyeksportowanym kompletem twoich danych osobistych będących w naszym posiadaniu, w tym całość tych dostarczonych przez ciebie. Możesz również zażądać usunięcia przez nas całości twoich danych osobistych w naszym posiadaniu. Nie dotyczy to żadnych danych które jesteśmy zobligowani zachować ze względów administracyjnych, prawnych albo bezpieczeństwa.</p><h2>Gdzie przesyłamy dane</h2><p>Komentarze gości mogą być sprawdzane za pomocą automatycznej usługi wykrywania spamu.</p><h2>Twoje dane kontaktowe</h2><h2>Informacje dodatkowe</h2><h3>Jak chronimy twoje dane?</h3><h3>Jakie mamy obowiązujące procedury w przypadku naruszenia prywatności danych</h3><h3>Od jakich stron trzecich otrzymujemy dane</h3><h3>Jakie automatyczne podejmowanie decyzji i/lub tworzenie profili przeprowadzamy z użyciem danych użytkownika</h3><h3>Branżowe wymogi regulacyjne dotyczące ujawniania informacji</h3>', 'Polityka prywatności', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2018-06-18 08:25:25', '2018-06-18 06:25:25', '', 3, 'http://localhost/idea2/2018/06/18/3-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2018-06-18 08:25:42', '2018-06-18 06:25:42', 'To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\n\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię pi&#241;a coladę (a także spacery, gdy pada deszcz).</blockquote>\n\n... lub taki:\n\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\n\nJako nowy użytkownik WordPressa powinieneś przejść do <a href="http://localhost/idea2/wp-admin/">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!', 'Przykładowa strona', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-06-18 08:25:42', '2018-06-18 06:25:42', '', 2, 'http://localhost/idea2/2018/06/18/2-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2018-06-18 09:07:15', '2018-06-18 07:07:15', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:12:"projects.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:14:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:14:"featured_image";i:11;s:10:"categories";i:12;s:4:"tags";i:13;s:15:"send-trackbacks";}s:11:"description";s:0:"";}', 'Projects', 'projects', 'publish', 'closed', 'closed', '', 'group_5b2758aabcd0b', '', '', '2018-06-18 10:23:30', '2018-06-18 08:23:30', '', 0, 'http://localhost/idea2/?post_type=acf-field-group&#038;p=77', 0, 'acf-field-group', '', 0),
(78, 1, '2018-06-18 09:07:15', '2018-06-18 07:07:15', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";}', 'Project', 'project_template', 'publish', 'closed', 'closed', '', 'field_5b2758bffe880', '', '', '2018-06-18 09:07:15', '2018-06-18 07:07:15', '', 77, 'http://localhost/idea2/?post_type=acf-field&p=78', 0, 'acf-field', '', 0),
(79, 1, '2018-06-18 09:07:15', '2018-06-18 07:07:15', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:4:"full";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image 1', 'project1_image', 'publish', 'closed', 'closed', '', 'field_5b275915fe881', '', '', '2018-06-18 09:15:56', '2018-06-18 07:15:56', '', 78, 'http://localhost/idea2/?post_type=acf-field&#038;p=79', 0, 'acf-field', '', 0),
(80, 1, '2018-06-18 09:07:15', '2018-06-18 07:07:15', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Project 1 Name', 'project1_name', 'publish', 'closed', 'closed', '', 'field_5b27594afe882', '', '', '2018-06-18 09:07:15', '2018-06-18 07:07:15', '', 78, 'http://localhost/idea2/?post_type=acf-field&p=80', 1, 'acf-field', '', 0),
(81, 1, '2018-06-18 09:07:15', '2018-06-18 07:07:15', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Project 1 Description', 'project1_description', 'publish', 'closed', 'closed', '', 'field_5b27597dfe883', '', '', '2018-06-18 09:07:15', '2018-06-18 07:07:15', '', 78, 'http://localhost/idea2/?post_type=acf-field&p=81', 2, 'acf-field', '', 0),
(85, 1, '2018-06-18 09:09:26', '2018-06-18 07:09:26', '', '1_27', '', 'inherit', 'open', 'closed', '', '1_27', '', '', '2018-06-19 10:00:36', '2018-06-19 08:00:36', '', 73, 'http://localhost/idea2/wp-content/uploads/2018/06/1_27.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2018-06-18 09:10:22', '2018-06-18 07:10:22', '', '1_40', '', 'inherit', 'open', 'closed', '', '1_40', '', '', '2018-06-18 09:10:26', '2018-06-18 07:10:26', '', 73, 'http://localhost/idea2/wp-content/uploads/2018/06/1_40.jpg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2018-06-18 09:10:57', '2018-06-18 07:10:57', '', 'Projects', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-06-18 09:10:57', '2018-06-18 07:10:57', '', 73, 'http://localhost/idea2/2018/06/18/73-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2018-06-18 10:27:25', '2018-06-18 08:27:25', '', 'Projects', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-06-18 10:27:25', '2018-06-18 08:27:25', '', 73, 'http://localhost/idea2/2018/06/18/73-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2018-06-18 11:14:03', '2018-06-18 09:14:03', '', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2018-06-18 14:58:15', '2018-06-18 12:58:15', '', 0, 'http://localhost/idea2/?page_id=89', 0, 'page', '', 0),
(90, 1, '2018-06-18 11:14:03', '2018-06-18 09:14:03', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2018-06-18 11:14:03', '2018-06-18 09:14:03', '', 89, 'http://localhost/idea2/2018/06/18/89-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2018-06-18 11:40:06', '2018-06-18 09:40:06', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:11:"contact.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Contact', 'contact', 'publish', 'closed', 'closed', '', 'group_5b277df114813', '', '', '2018-06-18 11:41:34', '2018-06-18 09:41:34', '', 0, 'http://localhost/idea2/?post_type=acf-field-group&#038;p=91', 0, 'acf-field-group', '', 0),
(92, 1, '2018-06-18 11:40:34', '2018-06-18 09:40:34', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Text below e-mail address', 'contact_text', 'publish', 'closed', 'closed', '', 'field_5b277e0393b3d', '', '', '2018-06-18 11:41:34', '2018-06-18 09:41:34', '', 91, 'http://localhost/idea2/?post_type=acf-field&#038;p=92', 0, 'acf-field', '', 0),
(93, 1, '2018-06-18 11:41:03', '2018-06-18 09:41:03', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2018-06-18 11:41:03', '2018-06-18 09:41:03', '', 89, 'http://localhost/idea2/2018/06/18/89-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2018-06-18 14:58:15', '2018-06-18 12:58:15', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2018-06-18 14:58:15', '2018-06-18 12:58:15', '', 89, 'http://localhost/idea2/2018/06/18/89-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2018-06-19 08:55:57', '2018-06-19 06:55:57', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Slide 1', 'slide1', 'publish', 'closed', 'closed', '', 'field_5b28a8aed3e75', '', '', '2018-06-19 08:58:05', '2018-06-19 06:58:05', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=95', 0, 'acf-field', '', 0),
(96, 1, '2018-06-19 08:55:57', '2018-06-19 06:55:57', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Slide 2', 'slide2', 'publish', 'closed', 'closed', '', 'field_5b28a8c6d3e76', '', '', '2018-06-19 08:58:54', '2018-06-19 06:58:54', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=96', 1, 'acf-field', '', 0),
(97, 1, '2018-06-19 08:55:57', '2018-06-19 06:55:57', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Slide 3', 'slide3', 'publish', 'closed', 'closed', '', 'field_5b28a8dbd3e77', '', '', '2018-06-19 08:58:54', '2018-06-19 06:58:54', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=97', 2, 'acf-field', '', 0),
(98, 1, '2018-06-19 08:55:58', '2018-06-19 06:55:58', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Slide 4', 'slide4', 'publish', 'closed', 'closed', '', 'field_5b28a8e8d3e78', '', '', '2018-06-19 08:58:54', '2018-06-19 06:58:54', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=98', 3, 'acf-field', '', 0),
(99, 1, '2018-06-19 08:55:58', '2018-06-19 06:55:58', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Slide 5', 'slide5', 'publish', 'closed', 'closed', '', 'field_5b28a8f3d3e79', '', '', '2018-06-19 08:58:54', '2018-06-19 06:58:54', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=99', 4, 'acf-field', '', 0),
(100, 1, '2018-06-19 08:59:41', '2018-06-19 06:59:41', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-19 08:59:41', '2018-06-19 06:59:41', '', 6, 'http://localhost/idea2/2018/06/19/6-revision-v1/', 0, 'revision', '', 0),
(101, 1, '2018-06-19 09:01:04', '2018-06-19 07:01:04', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-19 09:01:04', '2018-06-19 07:01:04', '', 6, 'http://localhost/idea2/2018/06/19/6-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2018-06-19 09:29:49', '2018-06-19 07:29:49', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Project 1 Image', 'projectimage1', 'publish', 'closed', 'closed', '', 'field_5b28ab57717ae', '', '', '2018-06-19 11:24:58', '2018-06-19 09:24:58', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=102', 12, 'acf-field', '', 0),
(103, 1, '2018-06-19 09:29:50', '2018-06-19 07:29:50', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Project 1 Name', 'project1', 'publish', 'closed', 'closed', '', 'field_5b28ab94717af', '', '', '2018-06-19 09:29:50', '2018-06-19 07:29:50', '', 8, 'http://localhost/idea2/?post_type=acf-field&p=103', 13, 'acf-field', '', 0),
(104, 1, '2018-06-19 09:29:50', '2018-06-19 07:29:50', 'a:10:{s:4:"type";s:9:"page_link";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:0:{}s:8:"taxonomy";a:0:{}s:10:"allow_null";i:0;s:14:"allow_archives";i:1;s:8:"multiple";i:0;}', 'Project 1 Link', 'project1_link', 'publish', 'closed', 'closed', '', 'field_5b28afb4717b0', '', '', '2018-06-19 09:29:50', '2018-06-19 07:29:50', '', 8, 'http://localhost/idea2/?post_type=acf-field&p=104', 14, 'acf-field', '', 0),
(105, 1, '2018-06-19 09:29:50', '2018-06-19 07:29:50', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Project 1 Link Name', 'project1-linkname', 'publish', 'closed', 'closed', '', 'field_5b28aff4717b1', '', '', '2018-06-19 09:29:50', '2018-06-19 07:29:50', '', 8, 'http://localhost/idea2/?post_type=acf-field&p=105', 15, 'acf-field', '', 0),
(106, 1, '2018-06-19 09:29:50', '2018-06-19 07:29:50', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Project 2 Image', 'projectimage2', 'publish', 'closed', 'closed', '', 'field_5b28b044717b2', '', '', '2018-06-19 11:24:58', '2018-06-19 09:24:58', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=106', 16, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(107, 1, '2018-06-19 09:29:50', '2018-06-19 07:29:50', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Project 2 Name', 'project2', 'publish', 'closed', 'closed', '', 'field_5b28b06d717b3', '', '', '2018-06-19 09:32:20', '2018-06-19 07:32:20', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=107', 17, 'acf-field', '', 0),
(108, 1, '2018-06-19 09:29:50', '2018-06-19 07:29:50', 'a:10:{s:4:"type";s:9:"page_link";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:0:{}s:8:"taxonomy";a:0:{}s:10:"allow_null";i:0;s:14:"allow_archives";i:1;s:8:"multiple";i:0;}', 'Project 2 Link', 'project2_link', 'publish', 'closed', 'closed', '', 'field_5b28b09a717b4', '', '', '2018-06-19 09:32:20', '2018-06-19 07:32:20', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=108', 18, 'acf-field', '', 0),
(109, 1, '2018-06-19 09:29:50', '2018-06-19 07:29:50', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Project 2 Link Name', 'project2-linkname', 'publish', 'closed', 'closed', '', 'field_5b28b0c4717b5', '', '', '2018-06-19 09:32:20', '2018-06-19 07:32:20', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=109', 19, 'acf-field', '', 0),
(110, 1, '2018-06-19 09:31:48', '2018-06-19 07:31:48', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Project 3 Image', 'projectimage3', 'publish', 'closed', 'closed', '', 'field_5b28b1094651c', '', '', '2018-06-19 11:24:58', '2018-06-19 09:24:58', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=110', 20, 'acf-field', '', 0),
(111, 1, '2018-06-19 09:31:48', '2018-06-19 07:31:48', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Project 3 Name', 'project3', 'publish', 'closed', 'closed', '', 'field_5b28b1214651d', '', '', '2018-06-19 09:39:39', '2018-06-19 07:39:39', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=111', 21, 'acf-field', '', 0),
(112, 1, '2018-06-19 09:31:48', '2018-06-19 07:31:48', 'a:10:{s:4:"type";s:9:"page_link";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:0:{}s:8:"taxonomy";a:0:{}s:10:"allow_null";i:0;s:14:"allow_archives";i:1;s:8:"multiple";i:0;}', 'Project 3 Link', 'project3_link', 'publish', 'closed', 'closed', '', 'field_5b28b1274651e', '', '', '2018-06-19 09:32:21', '2018-06-19 07:32:21', '', 8, 'http://localhost/idea2/?post_type=acf-field&#038;p=112', 22, 'acf-field', '', 0),
(113, 1, '2018-06-19 09:31:48', '2018-06-19 07:31:48', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Project 3 Link Name', 'project3-linkname', 'publish', 'closed', 'closed', '', 'field_5b28b0fb4651b', '', '', '2018-06-19 09:31:48', '2018-06-19 07:31:48', '', 8, 'http://localhost/idea2/?post_type=acf-field&p=113', 23, 'acf-field', '', 0),
(114, 1, '2018-06-19 09:34:33', '2018-06-19 07:34:33', '', '1_29', '', 'inherit', 'open', 'closed', '', '1_29', '', '', '2018-06-19 09:34:50', '2018-06-19 07:34:50', '', 6, 'http://localhost/idea2/wp-content/uploads/2018/06/1_29.jpg', 0, 'attachment', 'image/jpeg', 0),
(115, 1, '2018-06-19 09:34:44', '2018-06-19 07:34:44', '', '1_31', '', 'inherit', 'open', 'closed', '', '1_31', '', '', '2018-06-19 09:35:17', '2018-06-19 07:35:17', '', 6, 'http://localhost/idea2/wp-content/uploads/2018/06/1_31.jpg', 0, 'attachment', 'image/jpeg', 0),
(116, 1, '2018-06-19 09:35:44', '2018-06-19 07:35:44', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-19 09:35:44', '2018-06-19 07:35:44', '', 6, 'http://localhost/idea2/2018/06/19/6-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2018-06-19 09:42:10', '2018-06-19 07:42:10', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-19 09:42:10', '2018-06-19 07:42:10', '', 6, 'http://localhost/idea2/2018/06/19/6-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2018-06-19 09:53:25', '2018-06-19 07:53:25', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-19 09:53:25', '2018-06-19 07:53:25', '', 6, 'http://localhost/idea2/2018/06/19/6-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2018-06-19 10:00:41', '2018-06-19 08:00:41', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-06-19 10:00:41', '2018-06-19 08:00:41', '', 6, 'http://localhost/idea2/2018/06/19/6-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2018-06-19 10:24:54', '2018-06-19 08:24:54', '', 'About', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-06-19 10:24:54', '2018-06-19 08:24:54', '', 41, 'http://localhost/idea2/2018/06/19/41-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2018-06-19 11:07:52', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-06-19 11:07:52', '0000-00-00 00:00:00', '', 0, 'http://localhost/idea2/?post_type=acf-field-group&p=123', 0, 'acf-field-group', '', 0),
(124, 1, '2018-06-19 11:09:24', '2018-06-19 09:09:24', '', 'Footer', '', 'trash', 'closed', 'closed', '', 'footer__trashed', '', '', '2018-06-19 11:10:07', '2018-06-19 09:10:07', '', 0, 'http://localhost/idea2/?page_id=124', 0, 'page', '', 0),
(125, 1, '2018-06-19 11:09:24', '2018-06-19 09:09:24', '', 'Footer', '', 'inherit', 'closed', 'closed', '', '124-revision-v1', '', '', '2018-06-19 11:09:24', '2018-06-19 09:09:24', '', 124, 'http://localhost/idea2/2018/06/19/124-revision-v1/', 0, 'revision', '', 0),
(126, 1, '2018-06-19 13:09:58', '2018-06-19 11:09:58', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:10:"header.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Header', 'header', 'publish', 'closed', 'closed', '', 'group_5b28e4788f74e', '', '', '2018-06-19 13:15:07', '2018-06-19 11:15:07', '', 0, 'http://localhost/idea2/?post_type=acf-field-group&#038;p=126', 0, 'acf-field-group', '', 0),
(127, 1, '2018-06-19 13:10:21', '2018-06-19 11:10:21', 'a:10:{s:4:"type";s:9:"page_link";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:0:{}s:8:"taxonomy";a:0:{}s:10:"allow_null";i:0;s:14:"allow_archives";i:1;s:8:"multiple";i:0;}', 'Logo link', 'navimage-link', 'publish', 'closed', 'closed', '', 'field_5b28e48ba1371', '', '', '2018-06-19 13:10:21', '2018-06-19 11:10:21', '', 126, 'http://localhost/idea2/?post_type=acf-field&p=127', 0, 'acf-field', '', 0),
(128, 1, '2018-06-19 13:10:42', '2018-06-19 11:10:42', '', 'Header', '', 'publish', 'closed', 'closed', '', 'header', '', '', '2018-06-19 13:14:45', '2018-06-19 11:14:45', '', 0, 'http://localhost/idea2/?page_id=128', 0, 'page', '', 0),
(129, 1, '2018-06-19 13:10:42', '2018-06-19 11:10:42', '', 'Header', '', 'inherit', 'closed', 'closed', '', '128-revision-v1', '', '', '2018-06-19 13:10:42', '2018-06-19 11:10:42', '', 128, 'http://localhost/idea2/2018/06/19/128-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_termmeta`
--

CREATE TABLE IF NOT EXISTS `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Bez kategorii', 'bez-kategorii', 0),
(2, 'Menu 1', 'menu-1', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(13, 2, 0),
(16, 2, 0),
(17, 2, 0),
(18, 2, 0),
(19, 2, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'ideauser'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'false'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'session_tokens', 'a:2:{s:64:"897d75385595bd506a70708fb6202d1cd3ffad05fccdaaac6f8f063359931c3e";a:4:{s:10:"expiration";i:1529475690;s:2:"ip";s:3:"::1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36";s:5:"login";i:1529302890;}s:64:"8bcb0b1efd477c124cd820ed7b15f092e91a8ab4ce2d83b3398d7c7734955a2c";a:4:{s:10:"expiration";i:1529563742;s:2:"ip";s:3:"::1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36";s:5:"login";i:1529390942;}}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'closedpostboxes_page', 'a:0:{}'),
(23, 1, 'metaboxhidden_page', 'a:9:{i:0;s:23:"acf-group_5b277df114813";i:1;s:23:"acf-group_5b2758aabcd0b";i:2;s:23:"acf-group_5b238e80933b2";i:3;s:23:"acf-group_5b2398b0f1453";i:4;s:12:"revisionsdiv";i:5;s:16:"commentstatusdiv";i:6;s:11:"commentsdiv";i:7;s:7:"slugdiv";i:8;s:9:"authordiv";}'),
(24, 1, 'acf_user_settings', 'a:0:{}'),
(25, 1, 'meta-box-order_page', 'a:4:{s:15:"acf_after_title";s:0:"";s:4:"side";s:23:"submitdiv,pageparentdiv";s:6:"normal";s:131:"acf-group_5b238e80933b2,acf-group_5b212398b50d7,acf-group_5b2398b0f1453,revisionsdiv,commentstatusdiv,commentsdiv,slugdiv,authordiv";s:8:"advanced";s:0:"";}'),
(26, 1, 'screen_layout_page', '2'),
(27, 1, 'wp_user-settings', 'libraryContent=browse'),
(28, 1, 'wp_user-settings-time', '1529305855');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'ideauser', '$P$BYmSG72FrXvtbkbd4ce.xrn4f9OcjA0', 'ideauser', 'bartek9513@interia.pl', '', '2018-06-13 13:42:59', '', 0, 'ideauser');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=249;
--
-- AUTO_INCREMENT dla tabeli `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=978;
--
-- AUTO_INCREMENT dla tabeli `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT dla tabeli `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT dla tabeli `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
