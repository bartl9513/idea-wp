<?php

/*

Template Name: About

*/

get_header(); ?>

<div class="triangle">
    
            
    <section class="about2 container-fluid">
        <h1 class="about-title about-title2">About the company</h1>
        <div class="row justify-content-center sub-about">
            <div class="col-11 col-sm-6"><?php the_field('abouttextleft'); ?></div>
            <div class="col-11 col-sm-6"><img src="<?php bloginfo('template_directory'); ?>/img/1_40.jpg" alt="moving stairs"></div>
            
        </div>
        <div class="row justify-content-center sub-about">
            <div class="col-11 col-sm-6"><img src="<?php bloginfo('template_directory'); ?>/img/1_40.jpg" alt="moving stairs"></div>
            <div class="col-11 col-sm-6"><?php the_field('abouttextright'); ?></div>
        </div>
    </section>
    </div>
    <div class="quote-container">
       
        <div class="quote">
        <span class="quote-span1"><i class="fas fa-quote-left"></i></span>
        <span class="quote-span2"><i class="fas fa-quote-left"></i></span>
        <span class="quote-span3">~<?php the_field('quoteauthor'); ?></span>
        <?php the_field('quote'); ?></div>
    </div>
    <aside>
        <div class="aside-john">
            <h2 class="john"><?php the_field('textonimage-name'); ?></h2>
            <p><?php the_field('textonimage'); ?></p>
        </div>
    </aside>
    <div class="files-container">
        <h1 class="files-title">Files for download</h1>
        <div class="container-fluid">
            <div class="row files-row">
                <div class="col-12 col-sm-4 files-col"><div class="pdf"><i class="far fa-file-pdf"></i></div>Lorem ipsum dolor sit amet, consectetur adipisicing elit</div>
                <div class="col-12 col-sm-4 files-col"><div class="pdf"><i class="far fa-file-pdf"></i></div>Lorem ipsum dolor sit amet, consectetur adipisicing elit</div>
                <div class="col-12 col-sm-4 files-col"><div class="pdf"><i class="far fa-file-pdf"></i></div>Lorem ipsum dolor sit amet, consectetur adipisicing elit</div>
<!--
            </div>
            <div class="row files-row">
-->
                <div class="col-12 col-sm-4 files-col"><div class="pdf"><i class="far fa-file-pdf"></i></div>Lorem ipsum dolor sit amet, consectetur adipisicing elit</div>
                <div class="col-12 col-sm-4 files-col"><div class="pdf"><i class="far fa-file-pdf"></i></div>Lorem ipsum dolor sit amet, consectetur adipisicing elit</div>
                <div class="col-12 col-sm-4 files-col"><div class="pdf"><i class="far fa-file-pdf"></i></div>Lorem ipsum dolor sit amet, consectetur adipisicing elit</div>
            </div>
        </div>
    </div>


<?php get_footer(); ?>
